﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using PNetS;

namespace PNet.TestNetwork
{
    public class TestDispatchServer : ADispatchServer
    {
        private readonly List<TestClientDispatchClient> _pendingPlayers = new List<TestClientDispatchClient>();
        private readonly List<TestClientDispatchClient> _playerConnections = new List<TestClientDispatchClient>();

        private readonly List<TestRoomDispatchClient> _roomConnections = new List<TestRoomDispatchClient>();

        protected override void Initialize()
        {
        }

        protected override NetMessage GetMessage(int length)
        {
            return NetMessage.GetMessage(length);
        }

        protected override Task Shutdown(string reason)
        {
            foreach (var c in _playerConnections)
            {
                Disconnect(c, reason);
            }
            foreach (var c in _roomConnections)
            {
                Disconnect(c, reason);
            }
            return Task.FromResult(true);
        }

        internal async void BeginConnecting(TestClientDispatchClient client)
        {
            await Task.FromResult(true).ConfigureAwait(false);
            PlayerAttemptingConnection(client, new IPEndPoint(IPAddress.Loopback, 14000), player => client.DispatchPlayer = player, NetMessage.GetMessage(0));
            _pendingPlayers.Add(client);
        }

        protected override void AllowPlayerToConnect(Player player)
        {
            _pendingPlayers.FirstOrDefault(p => p.DispatchPlayer == player).AllowConnect(player);
        }

        internal async void FinishConnecting(TestClientDispatchClient client)
        {
            await Task.FromResult(true).ConfigureAwait(false);
            if (_pendingPlayers.Remove(client))
            {
                _playerConnections.Add(client);
            }
        }

        protected override void Disconnect(Player player, string reason)
        {
            Disconnect(GetConnect(player), reason);
        }

        private void Disconnect(TestClientDispatchClient playerConnection, string reason)
        {
            _pendingPlayers.Remove(playerConnection);
            _playerConnections.Remove(playerConnection);
            playerConnection.Disconnected(reason);

            RemovePlayer(playerConnection.DispatchPlayer);
        }

        internal async void Internal_Disconnect(TestClientDispatchClient playerConnection, string reason)
        {
            await Task.FromResult(true).ConfigureAwait(false);
            Disconnect(playerConnection, reason);
        }

        protected override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode, bool recycle)
        {
            SendToPlayer(GetConnect(player), msg, recycle);
        }

        protected override void SendToAllPlayers(NetMessage msg, ReliabilityMode mode)
        {
            SendToPlayers(msg, connection => true);
        }

        protected override void SendToAllPlayersExcept(Player player, NetMessage msg, ReliabilityMode mode)
        {
            SendToPlayers(msg, connection => connection.DispatchPlayer != player);
        }

        protected override void SendToRoom(Room room, NetMessage msg, ReliabilityMode mode)
        {
            SendToRoom(GetConnect(room), msg, true);
        }

        protected override void SendToOtherRooms(Room except, NetMessage msg, ReliabilityMode mode)
        {
            SendToRooms(msg, client => client.DispatchRoom != except);
        }

        protected override void SendToAllRooms(NetMessage msg, ReliabilityMode mode)
        {
            SendToRooms(msg, client => true);
        }

        void SendToPlayers(NetMessage msg, Predicate<TestClientDispatchClient> pred)
        {
            foreach (var c in _playerConnections)
            {
                if (pred(c))
                    SendToPlayer(c, msg, false);
            }
            NetMessage.RecycleMessage(msg);
        }

        void SendToRooms(NetMessage msg, Predicate<TestRoomDispatchClient> pred)
        {
            foreach (var c in _roomConnections)
            {
                if (pred(c))
                    SendToRoom(c, msg, false);
            }
            NetMessage.RecycleMessage(msg);
        }

        static void SendToPlayer(TestClientDispatchClient player, NetMessage msg, bool recycle)
        {
            var send = NetMessage.GetMessage(msg.LengthBytes);
            msg.Clone(send);
            player.ReceiveMessage(send);
            if (recycle)
                NetMessage.RecycleMessage(msg);
        }

        static void SendToRoom(TestRoomDispatchClient room, NetMessage msg, bool recycle)
        {
            var send = NetMessage.GetMessage(msg.LengthBytes);
            msg.Clone(send);
            room.ReceiveMessage(send);
            if (recycle)
                NetMessage.RecycleMessage(msg);
        }

        TestClientDispatchClient GetConnect(Player player)
        {
            return player.Connection as TestClientDispatchClient;
        }

        #region room
        TestRoomDispatchClient GetConnect(Room room)
        {
            return room.Connection as TestRoomDispatchClient;
        }

        internal void ReceiveMessage(TestClientDispatchClient client, NetMessage msg)
        {
            ConsumeData(client.DispatchPlayer, msg);
        }

        internal void BeginConnecting(TestRoomDispatchClient client)
        {
            string denyReason;
            Room room;
            if (!ApproveRoomConnection(new IPEndPoint(IPAddress.Loopback, 14000), GetMessage(36), out denyReason, out room))
                throw new Exception("Could not approve testnetwork room: " + denyReason);
            room.Connection = client;
            client.AllowConnection(room.Guid);
        }

        internal void ReceiveMessage(TestRoomDispatchClient client, NetMessage msg)
        {
            ConsumeData(client.DispatchRoom, msg);
        }

        internal async void Internal_Disconnect(TestRoomDispatchClient room, string reason)
        {
            await Task.FromResult(true).ConfigureAwait(false);
            Disconnect(room, reason);
        }

        private void Disconnect(TestRoomDispatchClient room, string reason)
        {
            _roomConnections.Remove(room);
            room.Disconnected(reason);

            RemoveRoom(room.DispatchRoom);
        }
        #endregion
    }
}
