using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;
using PNet.Structs;
using PNetC;
using Debug = PNetC.Debug;

namespace IntegrationTests
{
    [DebuggerDisplay("Player {Client.PlayerId} {Token} {Client.Server.Status}")]
    internal class TestClient
    {
        public int LoadDelay = 0;
        public readonly Client Client;
        public TestClient(Client client, string token)
        {
            Token = token;
            Client = client;
            Client.HailObject = new StringSerializer(token);
            Client.BeginRoomSwitch += ClientOnBeginRoomSwitch;
            Client.NetworkManager.ViewInstantiated += NetworkManagerOnViewInstantiated;
            Client.OnDisconnectedFromRoom += ClientOnOnDisconnectedFromRoom;
            var runThread = new Thread(Loop);
            runThread.Start();
        }

        private void ClientOnOnDisconnectedFromRoom(string s)
        {
            Debug.Log("Disconnected from room: {0}", s);
        }

        public string Token { get; private set; }

        public PNetS.Player DispatchPlayer { get; set; }
        public PNetR.Player RoomPlayer { get; set; }

        private void NetworkManagerOnViewInstantiated(NetworkView instance, Vector3F position, Vector3F rotation)
        {
            Debug.Log("Instantiated {0} at {1} {2}", instance, position, rotation);
        }

        private async void ClientOnBeginRoomSwitch(string s)
        {
            await Task.Delay(LoadDelay);
            Client.FinishRoomSwitch();
        }

        private bool quit = false;
        private void Loop()
        {
            while (!quit)
            {
                Client.ReadQueue();
                Thread.Sleep(1);
            }
        }

        /// <summary>
        /// connect this test client, and assert that it is connected (blocking)
        /// </summary>
        public PNetS.Player Connect(PNetS.Server server)
        {
            var config = new NetworkConfiguration();
            Client.StartConnection(config);

            Integration.WaitUntil(() => Client.Server.Status == ConnectionStatus.Connected, 10);

            Integration.WaitUntil(() =>
            {
                var dplayer = server.GetPlayer(Client.PlayerId);
                if (dplayer != null && dplayer.Id == Client.PlayerId)
                {
                    Assert.AreEqual(dplayer.UserData as string, Token);
                    DispatchPlayer = dplayer;
                    return true;
                }
                return false;
            });

            return DispatchPlayer;
        }

        public void Cleanup()
        {
            Client.Shutdown();
            Client.Cleanup();

            for (int j = 0; j < 1000; j++)
            {
                if (ConnectionStatus.Disconnected == Client.Server.Status)
                    break;
                Thread.Sleep(1);
            }
        }
    }
}