﻿using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;
using Room = PNetR.Room;
using Server = PNetS.Server;

namespace IntegrationTests
{
    [TestClass]
    public class RoomToDispatcherRpcTest
    {
        private Server _dispatcher;

        private static TestContext _context;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _context = context;
            Integration.SetupLoggers();
        }

        [TestInitialize]
        public void TestSetup()
        {
            _dispatcher = Integration.GetReadyServer();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            System.Console.WriteLine("------- cleanup -------");
            _dispatcher.Shutdown().GetAwaiter().GetResult();

            _dispatcher = null;
        }

        [TestMethod]
        public void RpcTest()
        {
            var rooms = Integration.GetMultipleConnectedRooms();

            var room = rooms[0];
            PNetS.Room sroom;
            if (!_dispatcher.TryGetRoom(room.Room.RoomId, out sroom))
                Assert.Fail("Could not get room");

            bool sroomCalled = false;
            sroom.SubscribeToRpc(1, msg =>
            {
                int value;
                msg.ReadInt32(out value);
                Assert.AreEqual(35, value);
                sroomCalled = true;
            });

            bool roomCalled = false;
            room.Room.Server.SubscribeToRpc(6, msg =>
            {
                int value;
                msg.ReadInt32(out value);
                Assert.AreEqual(85237745, value);
                roomCalled = true;
            });

            room.Room.Server.Rpc(1, 35);

            Integration.WaitUntil(() => sroomCalled);

            sroom.Rpc(6, 85237745);

            Integration.WaitUntil(() => roomCalled);

            rooms.Cleanup();
        }

        [TestMethod]
        public void Room2RoomRpcTest()
        {
            var rooms = Integration.GetMultipleConnectedRooms();

            var room = rooms[0];
            PNetS.Room sroom;
            if (!_dispatcher.TryGetRoom(room.Room.RoomId, out sroom))
                Assert.Fail("Could not get room");

            var room2 = rooms[1];
            PNetS.Room sroom2;
            if (!_dispatcher.TryGetRoom(room2.Room.RoomId, out sroom2))
                Assert.Fail("Could not get room 2");

            bool roomCalled = false;
            room.Room.Server.SubscribeToRpc(1, msg =>
            {
                roomCalled = true;
            });

            bool room2Called = false;
            room2.Room.Server.SubscribeToRpc(1, msg =>
            {
                var value = msg.ReadInt32();
                Assert.AreEqual(29, value);
                room2Called = true;
            });

            bool room3Called = false;
            rooms[2].Room.Server.SubscribeToRpc(1, msg =>
            {
                var value = msg.ReadInt32();
                Assert.AreEqual(29, value);
                room3Called = true;
            });

            room.Room.Server.Rpc(1, RpcMode.OthersOrdered, 29);

            Integration.WaitUntil(() => room2Called);
            Integration.WaitUntil(() => room3Called);

            Assert.IsFalse(roomCalled);

            rooms.Cleanup();
        }

        [TestMethod]
        public void NetUserDataSyncTest()
        {
            var rooms = Integration.GetMultipleConnectedRooms();
            var room = rooms[0];
            
            room.Room.ConstructNetData += () => new TestNetData();
            _dispatcher.ConstructNetData += () => new TestNetData();

            PNetS.Room sroom;
            if (!_dispatcher.TryGetRoom(room.Room.RoomId, out sroom))
                Assert.Fail("Could not get room");

            var room2 = rooms[1];
            room2.Room.ConstructNetData += () => new TestNetData();
            PNetS.Room sroom2;
            if (!_dispatcher.TryGetRoom(room2.Room.RoomId, out sroom2))
                Assert.Fail("Could not get room 2");

            var client = Integration.GetConnectedClient(_dispatcher);

            var dPlayer = _dispatcher.GetPlayer(client.Client.PlayerId);
            dPlayer.ChangeRoom(sroom);
            Integration.WaitUntil(() => client.Client.Room.RoomId == room.Room.RoomId);
            Integration.WaitUntil(() => room.Room.GetPlayer(client.Client.PlayerId) != null);
            var rPlayer = room.Room.GetPlayer(client.Client.PlayerId);

            dPlayer.TnUser<TestNetData>().Value = 8;
            dPlayer.SynchNetData();

            Integration.WaitUntil(() => rPlayer.TnUser<TestNetData>().Value == 8);
            rPlayer.TnUser<TestNetData>().Value = 932667;
            rPlayer.SynchNetData();
            Integration.WaitUntil(() => dPlayer.TnUser<TestNetData>().Value == 932667);

            rooms.Cleanup();
        }

        class TestNetData : INetSerializable
        {
            public int Value;
            public void OnSerialize(NetMessage message)
            {
                message.Write(Value);
            }

            public void OnDeserialize(NetMessage message)
            {
                Value = message.ReadInt32();
            }

            public int AllocSize { get { return 4; } }
        }
    }
}
