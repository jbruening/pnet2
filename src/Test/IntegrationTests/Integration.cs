﻿using System.Diagnostics;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PNet;
using PNetC;
using PNetC.Impl;
using PNetR.Impl;
using PNetS;
using PNetR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PNetS.Impl;
using Player = PNetS.Player;
using Room = PNetR.Room;
using Server = PNetS.Server;

#if !TCPDISPATCH
using ClientDispatchClient = PNetC.Impl.LidgrenDispatchClient;
using RoomDispatchClient = PNetR.Impl.LidgrenDispatchClient;
using DispatchRoomServer = PNetS.Impl.LidgrenRoomServer;
using DispatchPlayerServer = PNetS.Impl.LidgrenPlayerServer;
#else
using ClientDispatchClient = PNetC.Impl.TcpDispatchClient;
using RoomDispatchClient = PNetR.Impl.TcpDispatchClient;
using DispatchServer = PNetS.Impl.TcpDispatchServer;
#endif

namespace IntegrationTests
{
    static class Integration
    {
        private static TestLogger _clientLog;
        private static TestLogger _roomLog;
        private static TestLogger _dispatchLog;

        public static void SetupLoggers()
        {
            _clientLog = new TestLogger("client");
            _roomLog = new TestLogger("room");
            _dispatchLog = new TestLogger("dispatch");
            PNetC.Debug.Logger = _clientLog;
            PNetR.Debug.Logger = _roomLog;
            PNetS.Debug.Logger = _dispatchLog;
        }

        public static Task CheckClientLog()
        {
            _clientLog.Tcs.TrySetResult(true);
            return _clientLog.Tcs.Task;
        }

        public static Task CheckRoomLog()
        {
            _roomLog.Tcs.TrySetResult(true);
            return _roomLog.Tcs.Task;
        }

        public static Task CheckDispatchLog()
        {
            _dispatchLog.Tcs.TrySetResult(true);
            return _dispatchLog.Tcs.Task;
        }

        public static async Task CheckAllLogs()
        {
            await CheckClientLog();
            await CheckRoomLog();
            await CheckDispatchLog();
        }

        public static Server GetReadyServer(PNetS.ARoomServer roomImpl = null, PNetS.APlayerServer playerImpl = null)
        {
            var server = new Server(roomImpl ?? new DispatchRoomServer(), playerImpl ?? new DispatchPlayerServer());
            server.VerifyPlayer += ServerOnVerifyPlayer;
            var dispatchConfig = new PNetS.NetworkConfiguration();
            server.Initialize(dispatchConfig);
            return server;
        }

        public static void ServerOnVerifyPlayer(Player player, NetMessage netMessage)
        {
            var token = netMessage.ReadString();
            Guid res;
            if (!Guid.TryParse(token, out res))
                Assert.Fail("Expected a guid tostring for a token");
            player.UserData = token;
            player.AllowConnect();
        }

        public static TestRoom GetConnectedRoom(Server server, int port = 14100, int maxPlayers = 32, PNetR.ADispatchClient impl = null)
        {
            var roomConfig = new PNetR.NetworkConfiguration(listenPort: port, maximumPlayers:maxPlayers);
            var room = new Room(roomConfig, new PNetR.Impl.LidgrenRoomServer(), impl ?? new RoomDispatchClient());
            var troom = new TestRoom(room);
            room.StartConnection();

            WaitUntil(() => room.ServerStatus == ConnectionStatus.Connected, 10);

            WaitUntil(() =>
            {
                PNetS.Room r;
                if (server.TryGetRoom(room.RoomId, out r))
                {
                    troom.DispatchRoom = r;
                    return true;
                }
                return false;
            });


            return troom;
        }

        public static TestRoom[] GetMultipleConnectedRooms(int count = 20, int startPort = 14101)
        {
            var trooms = new TestRoom[count];
            //start the rooms
            for (int i = 0; i < trooms.Length; i++)
            {
                var config = new PNetR.NetworkConfiguration(listenPort: startPort + i, roomIdentifier: "room" + (i % 5));
                var room = new Room(config, new PNetR.Impl.LidgrenRoomServer(), new RoomDispatchClient());
                var troom = new TestRoom(room);

                room.StartConnection();
                trooms[i] = troom;
            }

            //then check for connection states
            for (int i = 0; i < trooms.Length; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    if (ConnectionStatus.Connected == trooms[i].Room.ServerStatus)
                        break;
                    Thread.Sleep(15);
                }

                Assert.AreEqual(ConnectionStatus.Connected, trooms[i].Room.ServerStatus);
            }
            return trooms;
        }

        public static void Cleanup(this TestRoom[] rooms)
        {
            for (int i = 0; i < rooms.Length; i++)
            {
                if (rooms[i] == null) continue;
                rooms[i].Room.Shutdown();
            }

            //then check for connection states
            for (int i = 0; i < rooms.Length; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    if (ConnectionStatus.Disconnected == rooms[i].Room.ServerStatus)
                        break;
                    Thread.Sleep(1);
                }

                Assert.AreEqual(ConnectionStatus.Disconnected, rooms[i].Room.ServerStatus);
            }
        }

        public static void WaitUntil(Func<bool> check, int timeout = 1)
        {
            var timer = new Stopwatch();
            timeout *= 1000;
            timer.Start();
            while (!check())
            {
                if (timer.ElapsedMilliseconds >= timeout)
                {
                    Assert.Fail("Timed out");
                }
                Thread.Sleep(10);
            }
        }

        public static async Task WaitUntilAsync(Func<bool> check, int timeout = 1)
        {
            var timer = new Stopwatch();
            timeout *= 1000;
            timer.Start();
            while (!check())
            {
                if (timer.ElapsedMilliseconds >= timeout)
                {
                    Assert.Fail("Timed out");
                }
                await Task.Delay(10);
            }
        }

        public static TestClient GetConnectedClient(Server server)
        {
            var client = new Client(new LidgrenRoomClient(), new ClientDispatchClient());
            var tokenId = Guid.NewGuid().ToString("N");
            var testRig = new TestClient(client, tokenId);
            var config = new PNetC.NetworkConfiguration();
            client.StartConnection(config);

            WaitUntil(() => client.Server.Status == ConnectionStatus.Connected, 10);

            WaitUntil(() =>
            {
                var dplayer = server.GetPlayer(client.PlayerId);
                if (dplayer != null && dplayer.Id == client.PlayerId)
                {
                    Assert.AreEqual(dplayer.UserData as string, tokenId);
                    testRig.DispatchPlayer = dplayer;
                    return true;
                }
                return false;
            });

            return testRig;
        }

        /// <summary>
        /// switch the client to the room. Ensured by verifying the player's token
        /// </summary>
        /// <param name="server"></param>
        /// <returns></returns>
        public static async Task<TestClient> GetConnectedClientAsync(Server server)
        {
            var client = new Client(new LidgrenRoomClient(), new ClientDispatchClient());
            var tokenId = Guid.NewGuid().ToString("N");
            var testRig = new TestClient(client, tokenId);
            var config = new PNetC.NetworkConfiguration();
            client.StartConnection(config);

            await WaitUntilAsync(() => client.Server.Status == ConnectionStatus.Connected, 10);

            await WaitUntilAsync(() =>
            {
                var dplayer = server.GetPlayer(client.PlayerId);
                if (dplayer != null && dplayer.Id == client.PlayerId)
                {
                    Assert.AreEqual(dplayer.UserData as string, tokenId);
                    testRig.DispatchPlayer = dplayer;
                    return true;
                }
                return false;
            });

            return testRig;
        }

        /// <summary>
        /// switch the client to the room. Ensured by verifying the player's token
        /// </summary>
        /// <param name="client"></param>
        /// <param name="room"></param>
        public static void SwitchRooms(TestClient client, TestRoom room)
        {
            client.DispatchPlayer.ChangeRoom(room.DispatchRoom);
            var switchToken = client.DispatchPlayer._switchToken;

            WaitUntil(() => client.Client.Room.RoomId == room.Room.RoomId);
            WaitUntil(() =>
            {
                var rp = room.Room.GetPlayer(client.Client.PlayerId);
                if (rp != null && rp.Token == switchToken)
                {
                    client.RoomPlayer = rp;
                    return true;
                }
                return false;
            });
        }

        public static async Task SwitchRoomsAsync(TestClient client, TestRoom room)
        {
            client.DispatchPlayer.ChangeRoom(room.DispatchRoom);
            var switchToken = client.DispatchPlayer._switchToken;

            await WaitUntilAsync(() => client.Client.Room.RoomId == room.Room.RoomId);
            await WaitUntilAsync(() =>
            {
                if (client.DispatchPlayer.CurrentRoomGuid != room.Room.RoomId)
                    return false;

                var rp = room.Room.GetPlayer(client.Client.PlayerId);

                if (rp != null && rp.Token == switchToken)
                {
                    client.RoomPlayer = rp;
                    return true;
                }
                return false;
            });
        }

        public static TestClient GetClient()
        {
            var client = new Client(new LidgrenRoomClient(), new ClientDispatchClient());
            var testRig = new TestClient(client, Guid.NewGuid().ToString("N"));
            return testRig;
        }
    }
}
