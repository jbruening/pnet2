﻿using System;
using System.Net;
using PNet;

namespace PNetC
{
    public abstract class ARoomClient
    {
        public enum waitForSwitchState
        {
            None,
            WaitForDc,
            WaitForRoomSwitch,
            Connecting,
        }

        protected internal Client Client;
        private waitForSwitchState _switchState;
        private DateTime? _badRoomSwitchTime;

        protected string QueuedRoomId { get; private set; }
        protected IPEndPoint QueuedRoomEndpoint { get; private set; }
        protected Guid SwitchToken { get; private set; }
        
        protected internal abstract void Start();
        protected abstract void SwitchRoom();
        protected abstract void ImplWaitForReconnect();

        internal void ReadQueue_Internal()
        {
            if (_badRoomSwitchTime.HasValue && DateTime.UtcNow > _badRoomSwitchTime)
            {
                Debug.LogError("Erronous roomswitch from room server");
                RaiseFailedRoomSwitch("Erronous roomswitch from room server");
            }
            ReadQueue();
        }
        protected abstract void ReadQueue();
        protected internal abstract void Disconnect(string reason);
        protected internal abstract NetMessage GetMessage(int size);

        public waitForSwitchState SwitchState
        {
            get { return _switchState; }
            protected set
            {
#if DEBUG
                Debug.Log("SwitchState {0}", value);
#endif
                _switchState = value;
            }
        }

        internal void SwitchRoom(IPEndPoint endPoint, string roomId, Guid switchToken)
        {
            _badRoomSwitchTime = null;
            QueuedRoomId = roomId;
            QueuedRoomEndpoint = endPoint;
            SwitchToken = switchToken;
            SwitchRoom();
        }

        internal void WaitForReconnect()
        {
            _badRoomSwitchTime = null; //just in case.
            SwitchState = waitForSwitchState.Connecting;
            ImplWaitForReconnect();
        }

        protected void RaiseBeginRoomSwitch()
        {
            Client.RaiseBeginRoomSwitch(QueuedRoomId);
        }

        protected void RaiseFinishedRoomSwitch()
        {
            Client.RaiseFinishedRoomSwitch();
        }

        protected void RaiseFailedRoomSwitch(string reason)
        {
            Client.RaiseFailedRoomSwitch(reason);
        }

        protected void RaiseDisconnectedFromRoom(string reason)
        {
            Client.RaiseDisconnectedFromRoom(reason);
        }

        internal virtual void InternalDisconnectedMessage(string reason)
        {
        }

        internal void InternalSendSceneViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            SendSceneViewMessage(msg, mode);
        }
        protected abstract void SendSceneViewMessage(NetMessage msg, ReliabilityMode mode);

        internal void InternalSendViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            SendViewMessage(msg, mode);
        }
        protected abstract void SendViewMessage(NetMessage msg, ReliabilityMode mode);

        internal void InternalSendStaticMessage(NetMessage msg, ReliabilityMode mode)
        {
            SendStaticMessage(msg, mode);
        }
        protected abstract void SendStaticMessage(NetMessage msg, ReliabilityMode mode);

        protected void WaitForRoomSwitch()
        {
            SwitchState = waitForSwitchState.WaitForRoomSwitch;
            RaiseBeginRoomSwitch();
        }

        protected void ConnectedToRoom(Guid roomId)
        {
            Client.Room.RoomId = roomId;

            if (SwitchState == waitForSwitchState.Connecting)
            {
                SwitchState = waitForSwitchState.None;
                RaiseFinishedRoomSwitch();
            }
        }

        protected void ConsumeData(NetMessage msg)
        {
            Client.Room.ConsumeData(msg);
        }

        protected void EarlyRoomSwitch()
        {
            _badRoomSwitchTime = DateTime.UtcNow + TimeSpan.FromSeconds(10);
        }
    }
}
