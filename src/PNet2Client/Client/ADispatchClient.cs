﻿using System;
using PNet;

namespace PNetC
{
    public abstract class ADispatchClient
    {
        protected internal Client Client { get; internal set; }

        protected internal abstract void Start();
        protected internal abstract void ReadQueue();
        protected internal abstract void Disconnect(string reason);
        protected internal abstract NetMessage GetMessage(int size);

        protected void FinalizeDisconnect()
        {
            Client.Server.Status = ConnectionStatus.Disconnected;
            Client.FinalizeDisconnect();
        }

        protected void RaiseFailedToConnect(string reason)
        {
            Client.RaiseFailedToConnect(reason);
        }

        protected void ConnectedToServer(ushort playerId)
        {
            Client.PlayerId = playerId;
            Debug.Log("Connected to server as {0}", playerId);
            Client.Server.Status = ConnectionStatus.Connected;
            Client.Server.StatusReason = "Connected";
            Client.RaiseConnectedToServer();
        }

        protected internal abstract void InternalSendMessage(NetMessage msg, ReliabilityMode mode);
        protected internal abstract void DisconnectIfStillConnected();

        protected void ConsumeData(NetMessage msg)
        {
            Client.Server.ConsumeData(msg);
        }

        protected void BeginDisconnect(string statusReason)
        {
            Client.PlayerId = 0;
            Client.Server.Status = ConnectionStatus.Disconnected;
            Client.Server.StatusReason = statusReason;
        }

        protected void UpdateStatus(ConnectionStatus status, string reason)
        {
            Client.Server.Status = status;
            Client.Server.StatusReason = reason;
        }
    }
}
