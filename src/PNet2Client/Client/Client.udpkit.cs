﻿using System.Diagnostics;
using System.IO;
using System.Threading;
#if UDPKIT
using System;
using System.Net;
using PNet.UdpKit;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UdpKit;
using PNet;

namespace PNetC
{
    partial class Client
    {
        private UdpSocket _roomSocket;
        private UdpConnection _roomConnection;
        private UdpSocket _dispatchSocket;

        partial void ImplementationConnectionStart()
        {
            UdpLog.SetWriter((level, message) =>
            {
                switch (level)
                {
                    case UdpLog.USER:
                    case UdpLog.INFO:
                        Debug.Log(message);
                        break;
                    case UdpLog.WARN:
                        Debug.LogWarning(message);
                        break;
                    case UdpLog.ERROR:
                        Debug.LogError(message);
                        break;
                }
            });

            _dispatchSocket = UdpSocket.Create<UdpPlatformManaged, NetMessageSerializer>();
            _dispatchSocket.Start(UdpEndPoint.Any);
            
            var addresses = Dns.GetHostAddresses(Configuration.ServerAddress);
            var addBytes = addresses[0].GetAddressBytes();
            var endpoint = new UdpEndPoint(new UdpIPv4Address(addBytes[0], addBytes[1], addBytes[2], addBytes[3]), (ushort)Configuration.ServerPort);
            if (HailObject != null)
            {
                var size = Serializer.SizeOf(HailObject);
                var msg = NetMessage.GetMessage(size);
                Serializer.Serialize(HailObject, msg);   
                _dispatchSocket.Connect(endpoint, msg);
            }
            else
                _dispatchSocket.Connect(endpoint);
        }

        partial void ImplementationDisconnect(string reason)
        {
            //TODO: send reason then wait for it to be sent (for timeout time) before closing
            var msg = NetMessage.GetMessage(reason.Length + 6);
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Unordered, BroadcastMode.Server, MsgType.Internal));
            msg.Write(DandPRpcs.DisconnectMessage);
            msg.Write(reason);
            if (Server.Connection != null)
                Server.Connection.Send(msg);
            if (_roomConnection != null)
                _roomConnection.Send(msg);
            _isAwaitingDisconnect = true;
            
            _dispatchSocket.Close();
            _roomSocket.Close();
        }

        private UdpEvent _ev;
        partial void ImplementationQueueRead()
        {
            Time = NetTime.Now;
            ReadRoomMessages();
            ReadDispatchMessages();
        }

        private readonly UdpEvent[] _drain = new UdpEvent[100];
        private bool _isAwaitingDisconnect;

        void ReadRoomMessages()
        {
            if (_roomSocket == null) return;
            var read = _roomSocket.Drain(_drain);
            for (int i = 0; i < read; i++)
            {
                var ev = _drain[i];
                var msg = ev.Object as NetMessage;
                switch (ev.EventType)
                {
                    case UdpEventType.ObjectReceived:
                        Room.ConsumeData(msg);
                        NetMessage.RecycleMessage(msg);
                        break;
                    case UdpEventType.ObjectDelivered:
                    case UdpEventType.ObjectRejected:
                        NetMessage.RecycleMessage(msg);
                        break;
                    case UdpEventType.ObjectLost:
                        if ((msg).Reliability != ReliabilityMode.Unreliable)
                            ev.Connection.Send(ev.Object);
                        break;
                    case UdpEventType.ConnectFailed:
                        OnFailedRoomSwitch.TryRaise("Room connection failed", Debug.Logger);
                        break;
                    case UdpEventType.ConnectRefused:
                        OnFailedRoomSwitch.TryRaise("Room connection refused", Debug.Logger);
                        break;
                    case UdpEventType.Connected:
                        _roomConnection = ev.Connection;
                        if (msg == null)
                            throw new NullReferenceException("Could not get room guid");
                        Guid rguid;
                        if (!msg.ReadGuid(out rguid))
                            throw new NullReferenceException("Could not get room guid");
                        Room.RoomId = rguid;

                        if (_switchState == waitForSwitchState.Connecting)
                        {
#if DEBUG
                            Debug.Log("finished connecting to room");
#endif
                            _switchState = waitForSwitchState.None;
                            OnFinishedRoomSwitch.TryRaise(Debug.Logger);
                        }
                        break;
                    case UdpEventType.Disconnected:
                        if (_isAwaitingDisconnect)
                            _isAwaitingDisconnect = false;
                        if (_switchState == waitForSwitchState.WaitForDc)
                        {
                            //fully disconnected. now we can tell users that they should switch rooms.
                            WaitForRoomSwitch();
                        }
                        else if (_switchState == waitForSwitchState.Connecting)
                        {
                            Debug.LogError("Failed to switch to room at {0}", _nendPoint);
                            OnFailedRoomSwitch.TryRaise("Failed to switch to room", Debug.Logger);
                        }
                        else
                            OnDisconnectedFromRoom.TryRaise("Disconnected from room", Debug.Logger);
                        break;
                }
            }
        }

        void ReadDispatchMessages()
        {
            if (_dispatchSocket == null) return;

            var read = _dispatchSocket.Drain(_drain);
            for (int i = 0; i < read; i++)
            {
                var ev = _drain[i];
                var msg = ev.Object as NetMessage;
                switch (ev.EventType)
                {
                    case UdpEventType.ObjectReceived:
                        Server.ConsumeData(msg);
                        NetMessage.RecycleMessage(msg);
                        break;
                    case UdpEventType.ObjectDelivered:
                    case UdpEventType.ObjectRejected:
                        NetMessage.RecycleMessage(msg);
                        break;
                    case UdpEventType.ObjectLost:
                        if ((msg).Reliability != ReliabilityMode.Unreliable)
                            ev.Connection.Send(ev.Object);
                        break;
                    case UdpEventType.ConnectFailed:
                        OnFailedToConnect.TryRaise("Server connection failed", Debug.Logger);
                        break;
                    case UdpEventType.ConnectRefused:
                        OnFailedToConnect.TryRaise("Server connection refused", Debug.Logger);
                        break;
                    case UdpEventType.Connected:
                        if (msg == null)
                            throw new NullReferenceException("Could not get player id");
                        if (msg.RemainingBits < 16)
                            throw new Exception("Could not read player id");
                        var id = msg.ReadUInt16();
                        PlayerId = id;
                        Server.Connection = ev.Connection ;
                        OnConnectedToServer.TryRaise(Debug.Logger);
                        break;
                    case UdpEventType.Disconnected:
                        if (_isAwaitingDisconnect)
                            _isAwaitingDisconnect = false;
                        Server.Connection = ev.Connection;
                        PlayerId = 0;
                        Server.Status = ConnectionStatus.Disconnected;
                        OnDisconnectedFromServer.TryRaise(Debug.Logger);
                        if (Configuration.DeleteNetworkInstantiatesOnDisconnect)
                        {
                            NetworkManager.DestroyAll();
                        }
                        break;
                }
            }
        }

        private IPEndPoint _nendPoint;
        partial void ImplementationSwitchRoom(IPEndPoint endPoint)
        {
            _nendPoint = endPoint;
            if (_roomConnection.IsConnected)
            {
#if DEBUG
                Debug.Log("begin room switch waiting for disconnect");
#endif
                _switchState = waitForSwitchState.WaitForDc;
                _roomConnection.Disconnect();
            }
            else
            {
#if DEBUG
                Debug.Log("begin room switch waiting for room switch");
#endif
                WaitForRoomSwitch();
            }

            if (Configuration.DeleteNetworkInstantiatesOnDisconnect)
                NetworkManager.DestroyAll();
        }

        private void WaitForRoomSwitch()
        {
            _switchState = waitForSwitchState.WaitForRoomSwitch;
            //and inform users that we're switching rooms.
            try
            {
                BeginRoomSwitch.Raise(_queuedSwitchRoomId);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        partial void ImplWaitForReconnect()
        {
            _switchState = waitForSwitchState.Connecting;
            var hail = NetMessage.GetMessage(16);
            hail.Write(_switchToken);
            _roomSocket.Connect(_nendPoint.ConvertEndPoint(), hail);
        }
    }
}
#endif