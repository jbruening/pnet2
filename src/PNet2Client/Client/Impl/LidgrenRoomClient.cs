﻿using System;
using System.Collections.Generic;
using Lidgren.Network;
using PNet;

namespace PNetC.Impl
{
    public class LidgrenRoomClient : ARoomClient
    {
        private NetClient _roomClient;
        private NetPeerConfiguration _roomConfiguration;

        private int _lastRoomFrameSize;

        protected internal override void Start()
        {
            _roomConfiguration = new NetPeerConfiguration(Client.Configuration.AppIdentifier);

            _roomClient = new NetClient(_roomConfiguration);
            _roomClient.Start();

#if DEBUG
            _roomConfiguration.SimulatedLoss = 0.001f;
            _roomConfiguration.SimulatedDuplicatesChance = 0.001f;
            _roomConfiguration.SimulatedMinimumLatency = 0.1f;
            _roomConfiguration.SimulatedRandomLatency = 0.01f;
#endif
        }

        protected override void SwitchRoom()
        {
            if (_roomClient.ConnectionStatus != NetConnectionStatus.Disconnected)
            {
                SwitchState = waitForSwitchState.WaitForDc;
                _roomClient.Disconnect(PtoDMsgs.RoomSwitch);
            }
            else
            {
                WaitForRoomSwitch();
            }
        }

        protected override void ReadQueue()
        {
            if (_roomClient == null) return;

            var messages = new List<NetIncomingMessage>(_lastRoomFrameSize * 2);
            _lastRoomFrameSize = _roomClient.ReadMessages(messages);

            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < messages.Count; i++)
            {
                var msg = NetMessage.GetMessage(messages[i].Data.Length);
                var msgType = messages[i].MessageType;
                var method = messages[i].DeliveryMethod;
                var senderConnection = messages[i].SenderConnection;
                messages[i].Clone(msg);
                msg.Sender = senderConnection;
                _roomClient.Recycle(messages[i]);

                if (msgType == NetIncomingMessageType.Data)
                {
                    ConsumeData(msg);
                }
                else if (msgType == NetIncomingMessageType.DebugMessage)
                {
                    Debug.Log(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.WarningMessage)
                {
                    Debug.LogWarning(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.ConnectionLatencyUpdated)
                {
                    Client.Room.Latency = msg.ReadFloat();
                }
                else if (msgType == NetIncomingMessageType.ErrorMessage)
                {
                    Debug.LogError(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.StatusChanged)
                {
                    var status = (NetConnectionStatus)msg.ReadByte();
                    var statusReason = msg.ReadString();
                    var lastStatus = Client.Room.Status;
                    Client.Room.Status = status.ToPNet();
                    Client.Room.StatusReason = statusReason;
                    Debug.Log("Room Status: {0}, {1}", status, statusReason);

                    if (status == NetConnectionStatus.Connected)
                    {
                        var serverConn = _roomClient.ServerConnection;
                        if (serverConn == null)
                            throw new NullReferenceException("Could not get room connection after connected");
                        var remsg = serverConn.RemoteHailMessage;
                        if (remsg == null)
                            throw new NullReferenceException("Could not get room guid");
                        byte[] gid;
                        if (!remsg.ReadBytes(16, out gid))
                            throw new Exception("Could not read room guid");

                        ConnectedToRoom(new Guid(gid));
                    }
                    else if (status == NetConnectionStatus.Disconnected)
                    {
                        if (SwitchState == waitForSwitchState.WaitForDc)
                        {
                            //fully disconnected. now we can tell users that they should switch rooms.
                            WaitForRoomSwitch();
                        }
                        else if (!Client._shuttingDown)
                        {
                            if (SwitchState == waitForSwitchState.Connecting)
                            {
                                Debug.LogError("Failed to switch to room at {0}: {1}", QueuedRoomEndpoint,
                                    statusReason);
                                RaiseFailedRoomSwitch(statusReason);
                            }
                            else
                            {
                                switch (statusReason)
                                {
                                    case PtoDMsgs.RoomSwitch:
                                        EarlyRoomSwitch();
                                        break;
                                    case DtoPMsgs.BadToken:
                                    case DtoPMsgs.NoRoom:
                                    case DtoPMsgs.TokenTimeout:
                                    case "Connection timed out":
                                    default:
                                        //todo: timeout?
                                        RaiseDisconnectedFromRoom(statusReason);
                                        break;
                                }
                            }
                        } 
                    }
                }
                else if (msgType == NetIncomingMessageType.Error)
                {
                    Debug.LogException(new Exception(msg.ReadString()) { Source = "Client.Lidgren.ReadRoomMessages [Lidgren Error]" }); //this should really never happen...
                }
                else
                {
                    Debug.LogWarning("Uknown message type {0}", msgType);
                }

                NetMessage.RecycleMessage(msg);
            }
        }

        protected override void ImplWaitForReconnect()
        {
            var hail = _roomClient.CreateMessage(16);
            hail.Write(SwitchToken.ToByteArray());
            _roomClient.Connect(QueuedRoomEndpoint, hail);
        }

        protected internal override void Disconnect(string reason)
        {
            _roomClient.Shutdown(reason);
        }

        protected internal override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessage(size);
        }

        protected override void SendSceneViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = _roomClient.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);
            if (seq == 2) seq = 3; //don't use player channel...

            _roomClient.SendMessage(lmsg, method, seq);
        }

        protected override void SendViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = _roomClient.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);

            _roomClient.SendMessage(lmsg, method, seq);
        }

        protected override void SendStaticMessage(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = _roomClient.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            var method = mode.RoomDelivery();
            _roomClient.SendMessage(lmsg, method);
        }
    }
}
