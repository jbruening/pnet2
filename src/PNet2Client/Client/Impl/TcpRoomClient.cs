﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using PNet;

namespace PNetC.Impl
{
    class TcpRoomClient : ARoomClient
    {
        private TcpClient tcp;
        private Thread _thread;

        protected internal override void Start()
        {
            if (tcp != null)
                tcp.Close();
            tcp = new TcpClient();
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            var client = ar.AsyncState as TcpClient;
            client.ReceiveBufferSize = 1024;
            client.SendBufferSize = 1024;
            client.NoDelay = true;
            client.LingerState = new LingerOption(true, 10);

            if (client != tcp)
            {
                throw new Exception("TcpClient Mismatch");
            }

            try
            {
                client.EndConnect(ar);
            }
            catch (Exception e)
            {
                client.Close();
                var msg = e.Message;
                EnqueueAction(() => RaiseDisconnectedFromRoom(msg));
                return;
            }

            _thread = new Thread(MessageLoop) { IsBackground = true, Name = "TcpRoomClient MessageLoop" };
            _thread.Start(client);
        }

        private void MessageLoop(object state)
        {
            var client = state as TcpClient;
            try
            {
                if (client != tcp)
                {
                    throw new Exception("TcpClient Mismatch");
                }

                Client.Room.Status = ConnectionStatus.Connecting;
                Client.Room.StatusReason = "Connection established, waiting for auth";
                Debug.Log("Authenticating with server");

                using (var networkStream = client.GetStream())
                {
                    //send auth...
                    //auth consists of sending the connection GUID
                    var msg = GetMessage(16);
                    msg.Write(SwitchToken);
                    msg.WriteSize();
                    networkStream.Write(msg.Data, 0, msg.LengthBytes);

                    
                    var buffer = new byte[ushort.MaxValue];
                    NetMessage dataBuffer = null;
                    var bufferSize = 0;
                    var lengthBuffer = new byte[2];
                    var bytesReceived = 0;
                    int readBytes;


                    var msgQueue = new Queue<NetMessage>();
                    while (!Client._shuttingDown)
                    {
                        readBytes = networkStream.Read(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            var readMessage = NetMessage.GetMessages(buffer, readBytes, ref bytesReceived,
                                ref dataBuffer, ref lengthBuffer, ref bufferSize, msgQueue.Enqueue);
                            if (readMessage >= 1)
                                break;
                        }
                        if (!client.Connected)
                            return;
                    }

                    if (msgQueue.Count == 0)
                        throw new Exception("Could not authenticate");
                    var authMsg = msgQueue.Dequeue();

                    var auth = authMsg.ReadBoolean();
                    authMsg.ReadPadBits();
                    if (!auth)
                    {
                        string reason;
                        if (!authMsg.ReadString(out reason))
                            reason = DtoPMsgs.BadToken;
                        EnqueueAction(() => RaiseDisconnectedFromRoom(reason));
                        return;
                    }
                    byte[] gid;
                    if (!authMsg.ReadBytes(16, out gid))
                        throw new Exception("Could not read room guid");

                    Client.Room.Status = ConnectionStatus.Connected;
                    Client.Room.StatusReason = "Connected";

                    EnqueueAction(() => ConnectedToRoom(new Guid(gid)));

                    _networkStream = networkStream;                    

                    //and drain the rest of messages that might have come after the auth
                    while (msgQueue.Count > 0)
                        EnqueueMessage(msgQueue.Dequeue());

                    while (!Client._shuttingDown)
                    {
                        readBytes = networkStream.Read(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            NetMessage.GetMessages(buffer, readBytes, ref bytesReceived, ref dataBuffer,
                                ref lengthBuffer, ref bufferSize, EnqueueMessage);
                        }
                        if (!client.Connected)
                            return;
                    }

                }
            }
            catch (ObjectDisposedException ode)
            {
                if (!Client._shuttingDown && ode.ObjectName != "System.Net.Sockets.NetworkStream")
                    Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
            }
            catch (IOException ioe)
            {
                if (!(ioe.InnerException is SocketException))
                    Debug.LogException(ioe);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                Client.Room.Status = ConnectionStatus.Disconnected;
                if (client != null)
                {
                    client.Close();
                }
                EnqueueAction(() => RaiseDisconnectedFromRoom(DtoPMsgs.BadToken));
            }
        }

        private readonly Queue<Action> _readActions = new Queue<Action>();
        private readonly Queue<NetMessage> _incomingMessages = new Queue<NetMessage>();
        private NetworkStream _networkStream;

        private void EnqueueMessage(NetMessage msg)
        {
            lock (_incomingMessages)
                _incomingMessages.Enqueue(msg);
        }

        private void EnqueueAction(Action action)
        {
            lock (_readActions)
                _readActions.Enqueue(action);
        }

        protected override void ImplWaitForReconnect()
        {
            Client.Room.Status = ConnectionStatus.Connecting;
            tcp.BeginConnect(QueuedRoomEndpoint.Address, QueuedRoomEndpoint.Port, ConnectCallback, tcp);
        }

        protected override void ReadQueue()
        {
            Action[] actions;
            lock (_readActions)
            {
                actions = _readActions.ToArray();
                _readActions.Clear();
            }
            foreach (var action in actions)
            {
                try
                {
                    action();
                }
                catch(Exception e)
                {
                    Debug.LogException(e);
                }
            }

            NetMessage[] messages;
            lock (_incomingMessages)
            {
                messages = _incomingMessages.ToArray();
                _incomingMessages.Clear();
            }
            foreach (var msg in messages)
            {
                try
                {
                    ConsumeData(msg);
                }
                catch(Exception e)
                {
                    Debug.LogException(e);
                }
                NetMessage.RecycleMessage(msg);
            }
        }

        protected override void SendSceneViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            WriteAsync(tcp.GetStream(), msg);
        }

        protected override void SendStaticMessage(NetMessage msg, ReliabilityMode mode)
        {
            WriteAsync(tcp.GetStream(), msg);
        }

        protected override void SendViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            WriteAsync(tcp.GetStream(), msg);
        }

        protected override void SwitchRoom()
        {
            if (tcp.Connected)
            {
                SwitchState = waitForSwitchState.WaitForDc;
                Disconnect(PtoDMsgs.RoomSwitch);
            }
            else
            {
                WaitForRoomSwitch();
            }
        }

        protected internal override void Disconnect(string reason)
        {
            Client.Room.Status = ConnectionStatus.Disconnecting;
            Client.Room.StatusReason = reason;

            var msg = GetMessage(reason.Length * 2 + 5);
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            msg.Write(RandPRpcs.DisconnectMessage);
            msg.Write(reason);
            WriteAsync(tcp.GetStream(), msg);
            tcp.Close();
        }

        protected internal override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessageSizePad(size);
        }

        internal override void InternalDisconnectedMessage(string reason)
        {
            if(SwitchState == waitForSwitchState.WaitForDc)
            {
                //fully disconnected. now we can tell users that they should switch rooms.
                WaitForRoomSwitch();
            }
            else if (!Client._shuttingDown)
            {
                if (SwitchState == waitForSwitchState.Connecting)
                {
                    Debug.LogError("Failed to switch to room at {0}: {1}", QueuedRoomEndpoint,
                        reason);
                    RaiseFailedRoomSwitch(reason);
                }
                else
                {
                    switch (reason)
                    {
                        case PtoDMsgs.RoomSwitch:
                            EarlyRoomSwitch();
                            break;
                        case DtoPMsgs.BadToken:
                        case DtoPMsgs.NoRoom:
                        case DtoPMsgs.TokenTimeout:
                        case "Connection timed out":
                        default:
                            //todo: timeout?
                            tcp.Close();
                            break;
                    }
                }
            }
        }

        class AsyncWriteInfo
        {
            public AsyncWriteInfo(NetworkStream stream, NetMessage msg)
            {
                Stream = stream;
                Message = msg;
            }

            public NetworkStream Stream { get; }
            public NetMessage Message { get; }
        }

        private void WriteAsync(NetworkStream stream, NetMessage msg)
        {
            stream.BeginWrite(msg.Data, 0, msg.LengthBytes, EndStreamWrite, new AsyncWriteInfo(stream, msg));
        }

        private void EndStreamWrite(IAsyncResult ar)
        {
            var awi = ar.AsyncState as AsyncWriteInfo;
            var stream = awi.Stream;

            try
            {
                stream.EndRead(ar);
            }
            catch (ObjectDisposedException ode)
            {
                if (stream != null && ode.ObjectName != "System.Net.Sockets.NetworkStream")
                {
                    Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
                    tcp.Close();
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                tcp.Close();
            }
            finally
            {
                NetMessage.RecycleMessage(awi.Message);
            }
            if (!tcp.Connected)
                tcp.Close();
        }
    }
}
