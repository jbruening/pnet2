﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using PNet;

namespace PNetC.Impl
{
    class MixedRoomClient : ARoomClient
    {
        private TcpClient _tcp;
        private UdpClient _udp;

        protected internal override void Start()
        {
            if (_tcp != null)
                _tcp.Close();
            if (_udp != null)
                _udp.Close();

            _tcp = new TcpClient();
        }

        protected override void SwitchRoom()
        {
            SwitchState = waitForSwitchState.WaitForDc;


        }

        protected override void ImplWaitForReconnect()
        {
            throw new NotImplementedException();
        }

        protected override void ReadQueue()
        {
            throw new NotImplementedException();
        }

        protected override void SendSceneViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected override void SendStaticMessage(NetMessage msg, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected override void SendViewMessage(NetMessage msg, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected internal override void Disconnect(string reason)
        {
            throw new NotImplementedException();
        }

        protected internal override NetMessage GetMessage(int size)
        {
            throw new NotImplementedException();
        }

        
    }
}
