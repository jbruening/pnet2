﻿using System.Threading;
#if UDP
using PNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Net.Sockets;
using PNet.UdpImpl;

namespace PNetC
{
    partial class Client
    {
        private const int HeartBeatTimeMs = 500;
        private NetMessage SendMessage = NetMessage.GetMessage(2048);
        private byte[] _dispatchRecvBuffer = new byte[2048];
        private byte[] _roomRecvBuffer = new byte[2048];
        ConstantPool<byte[]> _recvMessage = new ConstantPool<byte[]>(() => new byte[2048], (value) => value.Length == 2048, 100);

        Queue<NetMessage> SendQueue = new Queue<NetMessage>();
        Queue<NetMessage> ReceiveQueue = new Queue<NetMessage>();

        private Socket _roomSocket;
        private Socket _dispatchSocket;

        private Timer _heartbeat;
        private readonly IPEndPoint _listenerEndpoint = new IPEndPoint(IPAddress.Any, 0);
        private readonly IPEndPoint _roomListener = new IPEndPoint(IPAddress.Any, 0);
        private EndPoint _dispatchBeginSender = new IPEndPoint(IPAddress.Any, 0);
        private EndPoint _roomBeginSender = new IPEndPoint(IPAddress.Any, 0);
        private EndPoint _dispatchEndpoint;
        private EndPoint _roomEndpoint;

        partial void ImplementationConnectionStart()
        {

        }

        void Start()
        {
            _heartbeat = new Timer(Heartbeat);
            _dispatchSocket = new Socket(AddressFamily.InterNetwork,
                     SocketType.Dgram, ProtocolType.Udp);
            _roomSocket = new Socket(AddressFamily.InterNetwork,
                     SocketType.Dgram, ProtocolType.Udp);

            _dispatchSocket.Bind(_listenerEndpoint);
            _roomSocket.Bind(_roomListener);
            SetSocketIoControl(_dispatchSocket);
            SetSocketIoControl(_roomSocket);

            _heartbeat.Change(HeartBeatTimeMs, HeartBeatTimeMs);
            DispatchBeginReceive();
            RoomBeginReceive();
        }

        private void Heartbeat(object state)
        {
            PingDispatcher();
            PingRoom();
        }

        private void PingDispatcher()
        {
            if (_dispatchEndpoint == null)
                return;
            var message = GetPing();
            _dispatchSocket.SendTo(message.Data, message.LengthBytes, SocketFlags.None, _dispatchEndpoint);
            NetMessage.RecycleMessage(message);
        }

        void PingRoom()
        {
            if (_roomEndpoint == null)
                return;
            var message = GetPing();
            _roomSocket.SendTo(message.Data, message.LengthBytes, SocketFlags.None, _roomEndpoint);
            NetMessage.RecycleMessage(message);
        }

        private NetMessage GetPing()
        {
            var message = NetMessage.GetMessage(10);
            message.Write(RpcUtils.GetHeader(ReliabilityMode.Unreliable, BroadcastMode.Server, MsgType.Internal));
            message.Write(DandPRpcs.Ping);
            message.Write(DateTime.UtcNow);
        }

        private void DispatchBeginReceive()
        {
            _dispatchSocket.BeginReceiveFrom(_dispatchRecvBuffer, 0, 1024, SocketFlags.None, ref _dispatchBeginSender,
                DispatchReceiveCallback, _dispatchRecvBuffer);
        }

        private void DispatchReceiveCallback(IAsyncResult ar)
        {
            EndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            var receivedBytes = _dispatchSocket.EndReceiveFrom(ar, ref sender);
            var bytes = _recvMessage.Get();
            Buffer.BlockCopy(_dispatchRecvBuffer, 0, bytes, 0, receivedBytes);
            DispatchBeginReceive();

            ProcessIncoming(new MessageBuffer(sender as IPEndPoint, receivedBytes, bytes));
        }

        private void RoomBeginReceive()
        {
            _roomSocket.BeginReceiveFrom(_roomRecvBuffer, 0, 1024, SocketFlags.None, ref _roomBeginSender,
                RoomReceiveCallback, _roomRecvBuffer);
        }

        private void RoomReceiveCallback(IAsyncResult ar)
        {
            EndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            var receivedBytes = _roomSocket.EndReceiveFrom(ar, ref sender);
            var bytes = _recvMessage.Get();
            Buffer.BlockCopy(_roomRecvBuffer, 0, bytes, 0, receivedBytes);
            RoomBeginReceive();

            ProcessIncoming(new MessageBuffer(sender as IPEndPoint, receivedBytes, bytes));
        }

        private void ProcessIncoming(MessageBuffer buffer)
        {
            //our max packet size is 1024.
        }

        partial void ImplementationQueueRead()
        {
            ReadRoom();
            ReadDispatch();
        }

        private void ReadRoom()
        {
            
        }

        private void ReadDispatch()
        {
            
        }

        partial void ImplementationSwitchRoom(IPEndPoint endPoint)
        {

        }

        partial void ImplWaitForReconnect()
        {
            
        }

        partial void ImplementationDisconnect(string reason)
        {
            
        }

        #region static
        static void SetSocketIoControl(Socket socket)
        {
            try
            {
                //http://stackoverflow.com/a/7478498
                //ignore ICMP host unreachable messages
                const uint IOC_IN = 0x80000000;
                const uint IOC_VENDOR = 0x18000000;
                uint SIO_UDP_CONNRESET = IOC_IN | IOC_VENDOR | 12;
                socket.IOControl((int)SIO_UDP_CONNRESET, new[] { Convert.ToByte(false) }, null);
            }
            catch { }
        }
        #endregion
    }
}
#endif