﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PNet;

namespace PNetC
{
    public partial class NetworkView
    {
        private readonly Dictionary<int, Queue<AContinuation>> _continuations = new Dictionary<int, Queue<AContinuation>>();

        public void Rpc<TComp>(byte rpcId, RpcMode mode)
        {
            byte cid;
            if (!GetCompId<TComp>(out cid)) return;
            Rpc(cid, rpcId, mode);
        }

        public void Rpc<TComp>(byte rpcId, RpcMode mode, params object[] args)
            where TComp : class
        {
            byte cid;
            if (!GetCompId<TComp>(out cid)) return;
            Rpc(cid, rpcId, mode, args);
        }

        bool GetCompId<TComp>(out byte id)
        {
            if (!typeof(TComp).GetNetId(out id))
            {
                Debug.LogError("Could not get NetworkComponentAttribute from type {0}", typeof(TComp));
                return false;
            }
            return true;
        }

        public void Rpc(byte compId, byte rpcId, RpcMode mode)
        {
            var msg = StartMessage(compId, rpcId, mode, 0);
            SendMessage(msg, mode);
        }

        public void Rpc(byte compId, byte rpcId, RpcMode mode, params object[] args)
        {
            var size = 0;
            foreach (var arg in args)
            {
                if (arg == null)
                    throw new NullReferenceException("Cannot serialize null value");

                size += NetworkManager.Client.Serializer.SizeOf(arg);
            }

            var msg = StartMessage(compId, rpcId, mode, size);
            foreach (var arg in args)
            {
                NetworkManager.Client.Serializer.Serialize(arg, msg);
            }
            SendMessage(msg, mode);
        }

        public Continuation RpcContinueWith(byte compId, byte rpcId, RpcMode mode, params object[] args)
        {
            Rpc(compId, rpcId, mode, args);

            var cont = new Continuation(compId, rpcId);
            Enqueue(cont);
            return cont;
        }

        public Continuation<T> RpcContinueWith<T>(byte compId, byte rpcId, RpcMode mode, params object[] args)
        {
            Rpc(compId, rpcId, mode, args);

            var ser = NetworkManager.Client.Serializer;
            var cont = new Continuation<T>(compId, rpcId, message =>
            {
                if (ser.CanDeserialize(typeof (T)))
                    return (T) ser.Deserialize(typeof (T), message);
                return default(T);
            });
            Enqueue(cont);
            return cont;
        }

        void Enqueue(AContinuation continuation)
        {
            var id = (continuation.ComponentId << 8) | continuation.RpcId;
            Queue<AContinuation> queue;
            if (_continuations.TryGetValue(id, out queue))
            {
                queue.Enqueue(continuation);
            }
            else
            {
                queue = new Queue<AContinuation>();
                queue.Enqueue(continuation);
                _continuations[id] = queue;
            }
        }

        NetMessage StartMessage(byte cid, byte rpcId, RpcMode mode, int size)
        {
            var msg = NetworkManager.Client.Room.GetMessage(size + 5);

            msg.Write(RpcUtils.GetHeader(mode, MsgType.Netview));
            Id.OnSerialize(msg);
            msg.Write(cid);
            msg.Write(rpcId);
            return msg;
        }

        internal void SendMessage(NetMessage msg, RpcMode mode)
        {
            var reliable = mode.ReliabilityMode();

            ImplSendMessage(msg, reliable);
        }

        void ImplSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            NetworkManager.Client.SendViewMessage(msg, mode);
        }
    }
}
