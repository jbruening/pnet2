﻿using System;
using System.Collections.Generic;
using PNet;

namespace PNetC
{
    public partial class NetworkView : IComponentRpcProvider, IProxyCollection<INetComponentProxy>
    {
        private readonly Dictionary<Type, INetComponentProxy> _proxies = new Dictionary<Type, INetComponentProxy>();
        public readonly NetworkViewId Id;
        public readonly NetworkViewManager NetworkManager;
        public readonly string Resource;

        private ushort _ownerId;
        public ushort OwnerId
        {
            get { return _ownerId; }
            internal set
            {
                _ownerId = value;
                IsMine = _ownerId == NetworkManager.Client.PlayerId;
            }
        }
        public bool IsMine { get; private set; }

        /// <summary>
        /// size of the stream to serialize
        /// </summary>
        public int StreamSize { get; set; }

        public NetworkStateSynchronization StateSynchronization = NetworkStateSynchronization.Off;

        public event Action<NetMessage> ReceivedStream;
        public event Action<NetMessage> MoreInstantiateData;
        public event Action<byte> Destroyed;
        public event Action Hidden;

        internal NetworkView(NetworkViewId networkId, NetworkViewManager networkViewManager, string resource)
        {
            Id = networkId;
            NetworkManager = networkViewManager;
            Resource = resource;
        }

        #region proxy
        /// <summary>
        /// the value set from Proxy(IRoomProxy proxy)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Proxy<T>()
        {
            INetComponentProxy proxy;
            if (_proxies.TryGetValue(typeof(T), out proxy))
            {
                var ret = (T)proxy;
                proxy.CurrentRpcMode = RpcMode.OwnerOrdered;
                return ret;
            }
            return default(T);
        }

        /// <summary>
        /// the value set from Proxy(INetComponentProxy proxy)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Proxy<T>(RpcMode mode)
        {
            INetComponentProxy proxy;
            if (_proxies.TryGetValue(typeof(T), out proxy))
            {
                var ret = (T)proxy;
                proxy.CurrentRpcMode = mode;
                return ret;
            }
            return default(T);
        }

        /// <summary>
        /// Add a proxy to the network view to use for Proxy`T()
        /// </summary>
        /// <param name="proxy"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public void AddProxy(INetComponentProxy proxy)
        {
            if (proxy == null)
                throw new ArgumentNullException("proxy");
            proxy.NetworkView = this;

            var ptype = proxy.GetType();
            var interfaces = ptype.GetInterfaces();
            _proxies[ptype] = proxy;
            foreach (var face in interfaces)
            {
                _proxies[face] = proxy;
            }
        }

        public void RemoveProxy(INetComponentProxy proxy)
        {
            if (proxy == null)
                throw new ArgumentNullException("proxy");

            var ptype = proxy.GetType();
            var interfaces = ptype.GetInterfaces();
            _proxies.Remove(ptype);
            foreach (var face in interfaces)
            {
                _proxies.Remove(face);
            }
        }

        public void RemoveProxy<T>()
        {
            var ptype = typeof(T);
            var faces = ptype.GetInterfaces();
            _proxies.Remove(ptype);
            foreach (var face in faces)
            {
                _proxies.Remove(face);
            }
        }

        public void ClearProxies()
        {
            _proxies.Clear();
        }
        #endregion

        #region rpc subscription
        readonly Dictionary<int, RpcProcessor> _rpcProcessors = new Dictionary<int, RpcProcessor>();

        /// <summary>
        /// Subscribe to an rpc
        /// </summary>
        /// <param name="componentId"></param>
        /// <param name="rpcID">id of the rpc</param>
        /// <param name="rpcProcessor">action to process the rpc with</param>
        /// <param name="overwriteExisting">overwrite the existing processor if one exists.</param>
        /// <param name="defaultContinueForwarding">default value for info.continueForwarding</param>
        /// <returns>Whether or not the rpc was subscribed to. Will return false if an existing rpc was attempted to be subscribed to, and overwriteexisting was set to false</returns>
        public bool SubscribeToRpc(byte componentId, byte rpcID, Action<NetMessage> rpcProcessor, bool overwriteExisting = true, bool defaultContinueForwarding = true)
        {
            if (rpcProcessor == null)
                throw new ArgumentNullException("rpcProcessor", "the processor delegate cannot be null");

            var id = (componentId << 8) | rpcID;

            if (overwriteExisting)
            {
                _rpcProcessors[id] = new RpcProcessor(rpcProcessor, defaultContinueForwarding);
                return true;
            }

            if (_rpcProcessors.ContainsKey(id))
            {
                return false;
            }

            _rpcProcessors.Add(id, new RpcProcessor(rpcProcessor, defaultContinueForwarding));
            return true;
        }

        /// <summary>
        /// subscribe a function to an rpc id. The return value of func will be sent back to the client, into the rpc that called it with RpcContinueWith
        /// </summary>
        /// <param name="componentId"></param>
        /// <param name="rpcId"></param>
        /// <param name="func"></param>
        /// <param name="overwriteExisting"></param>
        /// <returns></returns>
        public bool SubscribeToFunc(byte componentId, byte rpcId, Func<NetMessage, object> func,
            bool overwriteExisting = true)
        {
            if (func == null)
                throw new ArgumentNullException("func", "the function delegate cannot be null");

            var id = (componentId << 8) | rpcId;

            if (overwriteExisting)
            {
                _rpcProcessors[id] = new RpcProcessor(func, false);
                return true;
            }
            if (_rpcProcessors.ContainsKey(id))
                return false;

            _rpcProcessors.Add(id, new RpcProcessor(func, false));
            return true;
        }

        /// <summary>
        /// Unsubscribe from an rpc
        /// </summary>
        /// <param name="componentId"></param>
        /// <param name="rpcID"></param>
        public void UnsubscribeFromRpc(byte componentId, byte rpcID)
        {
            var id = (componentId << 8) | rpcID;
            _rpcProcessors.Remove(id);
        }

        /// <summary>
        /// Unsubscribe all rpcs for component
        /// </summary>
        /// <param name="componentId"></param>
        public void UnsubscribeFromRpcs(byte componentId)
        {
            for (byte i = 0; i < byte.MaxValue; i++)
            {
                var id = (componentId << 8) | i;
                _rpcProcessors.Remove(id);
            }
        }

        public void ClearSubscriptions()
        {
            _rpcProcessors.Clear();
        }

        /// <summary>
        /// Subscribe all the marked rpcs on the supplied component
        /// </summary>
        /// <param name="component"></param>
        public void SubscribeMarkedRpcsOnComponent(Object component)
        {
            RpcSubscriber.SubscribeComponent<RpcAttribute>(this, component, NetworkManager.Client.Serializer, Debug.Logger);
        }

        #endregion

        internal void IncomingRpc(byte componentId, byte rpcId, NetMessage msg, SubMsgType sub)
        {
            var id = (componentId << 8) | rpcId;
            if (sub != SubMsgType.Out)
            {
                Queue<AContinuation> queue;
                if (!_continuations.TryGetValue(id, out queue)) return;
                if (queue.Count <= 0) return;
                
                var cont = queue.Dequeue();
                if (sub == SubMsgType.Reply)
                    cont.RunSuccess(msg);
                else if (sub == SubMsgType.Error)
                    cont.RunError(msg);
                return;
            }
            RpcProcessor processor;
            if (!_rpcProcessors.TryGetValue(id, out processor))
            {
                if (NetworkManager.ConcernedAboutUnhandledRpcs)
                    Debug.Log("Unhandled rpc {0} on component {1}", rpcId, componentId);
                return;
            }

            try
            {
                if (processor.Action != null)
                    try
                    {
                        processor.Action(msg);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e, "component {0} ActionRpc {1}", componentId, rpcId);
                    }
                else if (processor.Func != null)
                {
                    object ret;
                    try
                    {
                        ret = processor.Func(msg);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e, "component {0} FuncRpc {1}", componentId, rpcId);
                        ret = e;
                    }
                    //need to serialize and send back to the server.
                    ReturnFuncRpc(componentId, rpcId, ret);
                }
                else
                {
                    _rpcProcessors.Remove(id);
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        private void ReturnFuncRpc(byte componentId, byte rpc, object ret)
        {
            var msg = NetworkManager.Client.Room.GetMessage(5 + NetworkManager.Client.Serializer.SizeOf(ret));
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Netview,
                ret is Exception ? SubMsgType.Error : SubMsgType.Reply));
            Id.OnSerialize(msg);
            msg.Write(componentId);
            msg.Write(rpc);
            NetworkManager.Client.Serializer.Serialize(ret is Exception ? (ret as Exception).Message : ret, msg);
            SendMessage(msg, RpcMode.ServerOrdered);
        }

        internal void IncomingStream(NetMessage msg)
        {
            ReceivedStream.Raise(msg);
        }

        internal void MoreInstantiates(NetMessage msg)
        {
            MoreInstantiateData.Raise(msg);
        }

        /// <summary>
        /// make the engine network view serialize the stream
        /// </summary>
        public void SerializeStream()
        {
            if (NetworkManager.Client.Room.Status == ConnectionStatus.Connected)
            {
                var msg = NetworkManager.Client.Room.GetMessage(StreamSize);
                
                var reliable = StateSynchronization == NetworkStateSynchronization.Unreliable
                    ? ReliabilityMode.Unreliable
                    : ReliabilityMode.Ordered;

                msg.Write(RpcUtils.GetHeader(reliable, BroadcastMode.All, MsgType.Stream));
                msg.Write(Id.Id);
                _streamSerializing.Raise(msg);

                ImplSendMessage(msg, reliable);
            }
        }

        private Action<NetMessage> _streamSerializing;
        public void SetStreamSerialization(Action<NetMessage> serialization)
        {
            _streamSerializing = serialization;
        }

        void CleanForDestroy()
        {
            _rpcProcessors.Clear();
            _streamSerializing = null;
            _proxies.Clear();
        }

        internal void Destroying(byte reason)
        {
            CleanForDestroy();

            try
            {
                Destroyed.Raise(reason);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            Destroyed = null;
            Hidden = null;
        }

        internal void Hiding()
        {
            CleanForDestroy();

            try
            {
                Hidden.Raise();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            Destroyed = null;
            Hidden = null;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", Resource, Id.Id);
        }
    }
}