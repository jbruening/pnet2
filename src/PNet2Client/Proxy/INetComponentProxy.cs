﻿using PNet;

namespace PNetC
{
    public interface INetComponentProxy
    {
        /// <summary>
        /// The current thread's rpc mode.
        /// </summary>
        RpcMode CurrentRpcMode { get; set; }
        NetworkView NetworkView { get; set; }
    }
}