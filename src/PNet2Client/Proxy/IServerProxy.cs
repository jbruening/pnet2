﻿namespace PNetC
{
    public interface IServerProxy
    {
        Server Server { get; set; }
    }
}