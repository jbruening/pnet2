﻿using PNet;
using PNet.Structs;

namespace PNetC
{
    public partial class Room
    {
        internal void ConsumeData(NetMessage msg)
        {
            byte header;
            if (!msg.ReadByte(out header))
                return;
            MsgType msgType;
            BroadcastMode broadcast;
            ReliabilityMode reliability;
            SubMsgType sub;
            RpcUtils.ReadHeader(header, out reliability, out broadcast, out msgType, out sub);

            switch (msgType)
            {
                case MsgType.Internal:
                    ProcessInternal(broadcast, reliability, msg);
                    break;
                case MsgType.Static:
                    ProcessStatic(broadcast, reliability, msg);
                    break;
                case MsgType.Netview:
                    _client.NetworkManager.CallRpc(msg, sub);
                    break;
                case MsgType.Stream:
                    _client.NetworkManager.Stream(msg);
                    break;
                default:
                    Debug.LogWarning("Unsupported {0} byte message of type {1} from {2}", msg.LengthBytes - 1, msgType, this);
                    break;
            }
        }

        private void ProcessStatic(BroadcastMode broadcast, ReliabilityMode reliability, NetMessage msg)
        {
            byte rpc;
            if (!msg.ReadByte(out rpc))
                return;
            CallRpc(rpc, msg);
        }

        private void ProcessInternal(BroadcastMode broadcast, ReliabilityMode reliability, NetMessage msg)
        {
            byte rpc;
            if (!msg.ReadByte(out rpc))
            {
                Debug.LogError("Malformed internal room rpc");
                return;
            }

            switch (rpc)
            {
                case RandPRpcs.Instantiate:
                    ProcessInstantiate(msg);
                    break;
                case RandPRpcs.InstantiatePlus:
                    _client.NetworkManager.MoreInstantiates(msg);
                    break;
                case RandPRpcs.Destroy:
                    _client.NetworkManager.Destroy(msg);
                    break;
                case RandPRpcs.Hide:
                    _client.NetworkManager.Hide(msg);
                    break;
                case RandPRpcs.SceneObjectRpc:
                    if (msg.RemainingBits < 24L)
                    {
                        Debug.LogError("Malformed networked scene object rpc");
                    }
                    _client.SceneViewManager.CallRpc(msg.ReadUInt16(), msg.ReadByte(), msg);
                    break;
                case RandPRpcs.DisconnectMessage:
                    _client.RaiseDisconnectedFromRoom(msg.ReadString());
                    break;
            }
        }

        private void ProcessInstantiate(NetMessage msg)
        {
            if (msg.RemainingBits < 16)
            {
                Debug.LogError("Malformed instantiate - no id");
                return;
            }
            var id = msg.ReadUInt16();

            if (msg.RemainingBits < 16)
            {
                Debug.LogError("Malformed instantiate for {0} - no owner", id);
            }
            var owner = msg.ReadUInt16();
            
            string resource;
            if (!msg.ReadString(out resource))
            {
                Debug.LogError("Malformed instantiate for {0}, owner {1} - no resource", id, owner);
                return;
            }
            
            Vector3F pos;
            Vector3F rot;
            if (msg.RemainingBits < 96)
            {
                Debug.LogWarning("Malformed instantiate - no position/rotation");
                pos = new Vector3F();
                rot = new Vector3F();
            }
            else
            {
                pos = new Vector3F(msg);

                if (msg.RemainingBits < 96)
                {
                    Debug.LogWarning("Malformed instantiate - no rotation");
                    rot = new Vector3F();
                }
                else
                {
                    rot = new Vector3F(msg);
                }
            }
            
            _client.NetworkManager.Instantiate(id, owner, resource, pos, rot);

            //need to send instantiate finished message back.
            var nmsg = GetMessage(4);
            nmsg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            nmsg.Write(RandPRpcs.Instantiate);
            nmsg.Write(id);
            SendMessage(nmsg, ReliabilityMode.Ordered);
        }
    }
}
