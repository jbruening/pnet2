﻿#if UDPKIT
using PNet;
using UdpKit;

namespace PNetC
{
    public partial class Room
    {
        internal UdpConnection Connection { get; set; }

        partial void ImplementationSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            msg.Reliability = mode;
            Connection.Send(msg);
        }
    }
}
#endif