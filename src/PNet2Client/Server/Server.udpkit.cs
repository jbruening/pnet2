﻿#if UDPKIT
using UdpKit;
using PNet;

namespace PNetC
{
    public partial class Server
    {
        internal UdpConnection Connection { get; set; }

        partial void ImplementationSendMessage(NetMessage msg, ReliabilityMode mode)
        {
            msg.Reliability = mode;
            Connection.Send(msg);
        }
    }
}
#endif