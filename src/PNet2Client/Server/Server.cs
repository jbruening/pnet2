﻿using System;
using PNet;

namespace PNetC
{
    public partial class Server : IRpcProvider, IProxySingle<IServerProxy>
    {
        private readonly Client _client;
        internal Server(Client client)
        {
            _client = client;
        }

        public ConnectionStatus Status { get; internal set; }
        public string StatusReason { get; set; }

        private void CallRpc(byte rpcId, NetMessage msg)
        {
            var proc = _rpcProcessors[rpcId];
            if (proc == null)
            {
                Debug.LogWarning("Unhandled static server rpc {0}", rpcId);
            }
            else
                proc(msg);
        }

        readonly Action<NetMessage>[] _rpcProcessors = new Action<NetMessage>[256];

        public bool SubscribeToRpc(byte rpcId, Action<NetMessage> action)
        {
            _rpcProcessors[rpcId] = action;
            return true;
        }

        public void UnsubscribeRpc(byte rpcId)
        {
            _rpcProcessors[rpcId] = null;
        }

        public void SubscribeRpcsOnObject(object obj)
        {
            RpcSubscriber.SubscribeObject<RpcAttribute>(this, obj, _client.Serializer, Debug.Logger);
        }

        public void ClearSubscriptions()
        {
            for (int i = 0; i < _rpcProcessors.Length; i++)
            {
                _rpcProcessors[i] = null;
            }
        }

        internal void SendMessage(NetMessage msg, ReliabilityMode mode)
        {
            _client.SendDispatchMessage(msg, mode);
        }

        private IServerProxy _proxyObject;
        /// <summary>
        /// the value set from Proxy(IServerProxy proxy)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Proxy<T>()
        {
            return (T)_proxyObject;
        }
        /// <summary>
        /// set the proxy object to use when returning Proxy`T()
        /// </summary>
        /// <param name="proxy"></param>
        public void Proxy(IServerProxy proxy)
        {
            _proxyObject = proxy;
            if (_proxyObject != null)
                proxy.Server = this;
        }

        internal NetMessage GetMessage(int size)
        {
            return _client.DispatchGetMessage(size);
        }
    }
}