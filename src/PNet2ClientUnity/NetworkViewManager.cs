﻿using System.Collections;
using System.Collections.Generic;
using PNet;
using PNetC;
using UnityEngine;

namespace PNetU
{
    internal class NetworkViewManager
    {
        readonly Dictionary<NetworkViewId, NetworkView> _networkViews = new Dictionary<NetworkViewId, NetworkView>();

        public IEnumerable<KeyValuePair<NetworkViewId, NetworkView>>  Items
        {
            get { return _networkViews; }
        }

        internal void AddView(PNetC.NetworkView newView, NetworkView view)
        {
            NetworkView existing;
            if (_networkViews.TryGetValue(newView.Id, out existing))
                existing.OnNetDestroyed(255);
            _networkViews[newView.Id] = view;
            view.SetNetworkView(newView);
        }

        internal bool TryGetView(NetworkViewId viewId, out NetworkView view)
        {
            return _networkViews.TryGetValue(viewId, out view);
        }

        internal bool Remove(PNetC.NetworkView view)
        {
            NetworkView existing;
            if (_networkViews.TryGetValue(view.Id, out existing) && existing.NetView == view)
                return _networkViews.Remove(view.Id);
            PNetC.Debug.LogWarning("Attempted to remove view id {0}, but it is not the same view", view.Id);
            return false;
        }

        internal void Clear()
        {
            _networkViews.Clear();
        }
    }
}