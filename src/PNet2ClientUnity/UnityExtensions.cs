﻿using PNet;
using UnityEngine;
using NetworkStateSynchronization = UnityEngine.NetworkStateSynchronization;

namespace PNetU
{
    /// <summary>
    /// Extensions for unity stuff
    /// </summary>
    public static class UnityExtensions
    {
        /// <summary>
        /// Serialize the quaternion to the message
        /// </summary>
        /// <param name="quaternion"></param>
        /// <param name="message"></param>
        public static void Serialize(this Quaternion quaternion, NetMessage message)
        {
            message.Write(quaternion.x);
            message.Write(quaternion.y);
            message.Write(quaternion.z);
            message.Write(quaternion.w);
        }

        /// <summary>
        /// Deserialize the quaternion to the message
        /// </summary>
        /// <param name="quaternion"></param>
        /// <param name="message"></param>
        public static Quaternion Deserialize(this Quaternion quaternion, NetMessage message)
        {
            quaternion.x = message.ReadFloat();
            quaternion.y = message.ReadFloat();
            quaternion.z = message.ReadFloat();
            quaternion.w = message.ReadFloat();
            return quaternion;
        }

        /// <summary>
        /// Serialize the vector3 to the message
        /// </summary>
        /// <param name="vector3"></param>
        /// <param name="message"></param>
        public static void Serialize(this Vector3 vector3, NetMessage message)
        {
            message.Write(vector3.x);
            message.Write(vector3.y);
            message.Write(vector3.z);
        }

        /// <summary>
        /// Deserialize the vector3 from the message
        /// </summary>
        /// <param name="vector3"></param>
        /// <param name="message"></param>
        public static Vector3 Deserialize(this Vector3 vector3, NetMessage message)
        {
            vector3.x = message.ReadFloat();
            vector3.y = message.ReadFloat();
            vector3.z = message.ReadFloat();
            return vector3;
        }

        /// <summary>
        /// Get the 3 bytes
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static byte[] RGBBytes(this Color color)
        {
            Color32 col32 = color;

            byte[] bytes = new byte[3];
            bytes[0] = col32.r;
            bytes[1] = col32.g;
            bytes[2] = col32.b;

            return bytes;
        }

        /// <summary>
        /// Get the 4 bytes
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static byte[] ARGBBytes(this Color color)
        {
            Color32 col32 = color;

            byte[] bytes = new byte[4];
            bytes[0] = col32.a;
            bytes[1] = col32.r;
            bytes[2] = col32.g;
            bytes[3] = col32.b;

            return bytes;
        }

        /// <summary>
        /// Get the 3 bytes
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static byte[] RGBBytes(this Color32 color)
        {
            byte[] bytes = new byte[3];
            bytes[0] = color.r;
            bytes[1] = color.g;
            bytes[2] = color.b;

            return bytes;
        }

        /// <summary>
        /// Get the 4 bytes
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static byte[] ARGBBytes(this Color32 color)
        {
            byte[] bytes = new byte[4];
            bytes[0] = color.a;
            bytes[1] = color.r;
            bytes[2] = color.g;
            bytes[3] = color.b;

            return bytes;
        }

        /// <summary>
        /// add serializers for the unity types
        /// </summary>
        /// <param name="serializer"></param>
        public static void AddSerializers(SerializationManager serializer)
        {
            serializer.Add(typeof (Vector3), new Serializer(o => 12, (o, message) =>
            {
                var v3 = (Vector3) o;
                message.Write(v3.x);
                message.Write(v3.y);
                message.Write(v3.z);
            }), new Deserializer(message => new Vector3(message.ReadFloat(), message.ReadFloat(), message.ReadFloat())));
            
            serializer.Add(typeof (Quaternion), new Serializer(o => 16, (o, message) =>
            {
                var v3 = (Quaternion) o;
                message.Write(v3.x);
                message.Write(v3.y);
                message.Write(v3.z);
                message.Write(v3.w);
            }),
                new Deserializer(
                    message =>
                        new Quaternion(message.ReadFloat(), message.ReadFloat(), message.ReadFloat(),
                            message.ReadFloat())));
            
            serializer.Add(typeof(Color), new Serializer(o => 4, (o, message) =>
            {
                Color32 col32 = (Color) o;
                message.Write(col32.r);
                message.Write(col32.g);
                message.Write(col32.b);
                message.Write(col32.a);
            }), new Deserializer(message =>
            {
                var col32 = new Color32(message.ReadByte(), message.ReadByte(), message.ReadByte(), message.ReadByte());
                return (Color) col32;
            }));

            serializer.Add(typeof (Ray), new Serializer(o => 24, (o, message) =>
            {
                var ray = (Ray) o;
                message.Write(ray.origin.x);
                message.Write(ray.origin.y);
                message.Write(ray.origin.z);
                message.Write(ray.direction.x);
                message.Write(ray.direction.y);
                message.Write(ray.direction.z);
            }),
                new Deserializer(
                    message => new Ray(new Vector3(message.ReadFloat(), message.ReadFloat(), message.ReadFloat()),
                        new Vector3(message.ReadFloat(), message.ReadFloat(), message.ReadFloat()))));
        }
    }
}
