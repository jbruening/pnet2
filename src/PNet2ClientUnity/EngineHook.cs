﻿extern alias pnet;
using System;
using System.Collections.Generic;
using PNet;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace PNetU
{
    /// <summary>
    /// network hooking into the Update method of unity
    /// </summary>
    internal class EngineHook : MonoBehaviour
    {
        private PNetC.Client _peer;
        readonly NetworkViewManager _manager = new NetworkViewManager();
        internal NetworkViewManager Manager { get { return _manager; } }
        private bool _destroyed;

        public static EngineHook Instance { get; private set; }

        //because we're dontdestroyonload, if we're ever destroyed
        //we shouldn't attempt to recreate an instance if requested
        //because someone might be erronously calling us.
        public static EngineHook Create(PNetC.Client peer)
        {
            if (ValidInstance)
            {
                Instance.SetPeer(peer);
                return Instance;
            }
            //otherwise its in the process of destroying...

            var gobj = new GameObject("PNetU Singleton Engine Hook");
            Instance = gobj.AddComponent<EngineHook>();
            Instance.SetPeer(peer);
            DontDestroyOnLoad(gobj);

            return Instance;
        }

        /// <summary>
        /// if the instance is valid. If this is not used, calls to Instance can cause unity to leak objects
        /// </summary>
        public static bool ValidInstance
        {
            get
            {
                if (Instance != null && !Instance._destroyed)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// not changeable, just to show current status
        /// </summary>
        public string StatusReason;
        /// <summary>
        /// not changeable, just to show current status
        /// </summary>
        public ConnectionStatus Status;
        /// <summary>
        /// not changeable, just to show current status
        /// </summary>
        public string RoomStatusReason;
        /// <summary>
        /// not changeable, just to show current status
        /// </summary>
        public ConnectionStatus RoomStatus;

        void Awake()
        {
            PNetC.Debug.Logger = gameObject.AddComponent<UnityDebugLogger>();
        }

        void SetPeer(PNetC.Client peer)
        {
            if (_peer != null)
                _peer.NetworkManager.ViewInstantiated -= Instantiate;
            _peer = peer;
            _peer.NetworkManager.ViewInstantiated += Instantiate;
        }

        /// <summary>
        /// Run every frame, as long as the script is enabled
        /// </summary>
        void Update()
        {
            Net.Peer.ReadQueue();
            StatusReason = Net.StatusReason;
            Status = Net.Status;
            RoomStatusReason = Net.Peer.Room.StatusReason;
            RoomStatus = Net.Peer.Room.Status;
        }

        void OnDestroy()
        {
            _destroyed = true;
            Net.Disconnect();
            //run some cleanup too, just in case
            Net.CleanupEvents();
            Manager.Clear();
        }

        internal static Dictionary<string, GameObject> ResourceCache = new Dictionary<string, GameObject>();

        void Instantiate(PNetC.NetworkView newView, PNet.Structs.Vector3F location, PNet.Structs.Vector3F rotation)
        {
            GameObject gobj;
            bool isCached = false;
            if (Net.ResourceCaching && (isCached = ResourceCache.ContainsKey(newView.Resource)))
                gobj = ResourceCache[newView.Resource];
            else
                gobj = Resources.Load(newView.Resource) as GameObject;

            if (Net.ResourceCaching && !isCached)
                ResourceCache.Add(newView.Resource, gobj);

            Quaternion quat = Quaternion.Euler(rotation.X, rotation.Y, rotation.Z);
            if (Net.ConvertRotation != null)
                quat = Net.ConvertRotation(rotation);
            var instance = (GameObject)Instantiate(gobj, new Vector3(location.X, location.Y, location.Z), quat);

            if (instance == null)
            {
                Debug.LogWarning("could not find prefab " + newView.Resource + " to instantiate");
                instance = new GameObject("BROKEN NETWORK PREFAB " + newView.Id);
            }

            Debug.Log(string.Format("network instantiate of {0}. Loc: {1} Rot: {2}", newView.Resource, location, rotation), instance);

            //look for a networkview..
            var view = instance.GetComponent<NetworkView>();
            if (view == null)
                view = instance.AddComponent<NetworkView>();

            _manager.AddView(newView, view);

            var nBehaviours = instance.GetComponents<NetBehaviour>();

            foreach (var behave in nBehaviours)
            {
                behave.NetView = view;
            }

            view.DoOnFinishedCreation();
        }
    }
}