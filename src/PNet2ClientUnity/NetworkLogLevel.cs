﻿namespace PNetU
{
    public enum NetworkLogLevel
    {
        Off = 0,
        Info,
        Full
    }
}
