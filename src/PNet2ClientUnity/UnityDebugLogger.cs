﻿using System;
using UnityEngine;
using ILogger = PNet.ILogger;

namespace PNetU
{
    /// <summary>
    /// Logs to standard UnityEngine.Debug class
    /// </summary>
    internal sealed class UnityDebugLogger : MonoBehaviour, ILogger
    {
        private bool _isDebug;

        void Start()
        {
            _isDebug = UnityEngine.Debug.isDebugBuild;
        }

        void ILogger.Info(string info, params object[] args)
        {
            if (_isDebug || Net.LogLevel >= NetworkLogLevel.Info)
                UnityEngine.Debug.Log(string.Format(info, args));
        }

        void ILogger.Warning(string info, params object[] args)
        {
            UnityEngine.Debug.LogWarning(string.Format(info, args));
        }

        void ILogger.Error(string info, params object[] args)
        {
            UnityEngine.Debug.LogError(string.Format(info, args));
        }

        public void Exception(Exception exception, string info, params object[] args)
        {
            UnityEngine.Debug.LogError(string.Format(string.Format(info, args) + " ex: {0}", exception));
        }
    }
}
