﻿using System;
using System.Text;
using PNetC;
using UnityEngine;
using PNet;
using PNetU;

/// <summary>
/// Objects that exist in a scene with pre-synchronized network id's
/// </summary>
[AddComponentMenu("PNet/Networked Scene Object")]
public abstract class NetworkedSceneObject : MonoBehaviour
{
    /// <summary>
    /// The scene/room Network ID of this item. Should unique per room
    /// </summary>
    public int NetworkID = 0;

    PNetC.NetworkedSceneView _sceneView;

    /// <summary>
    /// If you override, you need to run SetupPNetC, probably first.
    /// </summary>
    protected void Awake()
    {
        SetupPNetC();
    }

    /// <summary>
    /// Only do this if you override awake
    /// </summary>
    protected void SetupPNetC()
    {
        if (Net.Peer != null)
            _sceneView = new PNetC.NetworkedSceneView((ushort)NetworkID, Net.Peer);
    }


    #region RPC Processing
    /// <summary>
    /// Subscribe to an rpc
    /// </summary>
    /// <param name="rpcID">id of the rpc</param>
    /// <param name="rpcProcessor">action to process the rpc with</param>
    /// <param name="overwriteExisting">overwrite the existing processor if one exists.</param>
    /// <returns>Whether or not the rpc was subscribed to. Will return false if an existing rpc was attempted to be subscribed to, and overwriteexisting was set to false</returns>
    public bool SubscribeToRpc(byte rpcID, Action<NetMessage> rpcProcessor, bool overwriteExisting = true)
    {
        return _sceneView.SubscribeToRpc(rpcID, rpcProcessor, overwriteExisting);
    }

    /// <summary>
    /// Unsubscribe from an rpc
    /// </summary>
    /// <param name="rpcID"></param>
    public void UnsubscribeFromRpc(byte rpcID)
    {
        _sceneView.UnsubscribeRpc(rpcID);
    }
    #endregion

    #region proxy
    /// <summary>
    /// the value set from Proxy(IServerProxy proxy)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T Proxy<T>()
    {
        return _sceneView.Proxy<T>();
    }

    /// <summary>
    /// set the proxy object to use when returning Proxy`T()
    /// </summary>
    /// <param name="proxy"></param>
    public void Proxy(ISceneViewProxy proxy)
    {
        _sceneView.Proxy(proxy);
    }
    #endregion

    /// <summary>
    /// Send an rpc to the server
    /// </summary>
    /// <param name="rpcID"></param>
    /// <param name="args"></param>
    public void Rpc(byte rpcID, params object[] args)
    {
        _sceneView.Rpc(rpcID, args);
    }

    /// <summary>
    /// serialize this into a string
    /// </summary>
    /// <returns></returns>
    public string Serialize()
    {
        var serObj = new PNetC.NetworkedSceneView((ushort)NetworkID);
        var sb = new StringBuilder();
        sb.Append(serObj.Serialize());
        sb.Append("type:").Append(GetType().Name).AppendLine(";");
        sb.Append("data:").Append(SerializeObjectData()).AppendLine(";");
        sb.Append("pos:").Append(transform.position).AppendLine(";");
        sb.Append("rot:").Append(transform.rotation).AppendLine(";");

        return sb.ToString();
    }

    /// <summary>
    /// Get the data to serialize for this scene object
    /// </summary>
    /// <returns></returns>
    protected abstract string SerializeObjectData();
}