﻿using System;
using PNet;
using PNetC;
using PNetC.Impl;
using UnityEngine;

namespace PNetU
{
    /// <summary>
    /// Networking class. Is basically a static class -> PNetC instance binding
    /// </summary>
    public static class Net
    {
        /// <summary>
        /// resource caching for instantiation
        /// </summary>
        public static bool ResourceCaching;

        /// <summary>
        /// the PNetC.Net instance used by this static class
        /// </summary>
        public static Client Peer { get; private set; }

        private static EngineHook Hook { get; set; }

        /// <summary>
        /// whether or not the system is initialized
        /// </summary>
        public static bool Initialized { get; private set; }

        /// <summary>
        /// Initialize networking with lidgren for room and dispatcher
        /// </summary>
        public static void Initialize()
        {
            Initialize(new Client(new LidgrenRoomClient(), new LidgrenDispatchClient()));
        }
        /// <summary>
        /// Initialize networking with the specified client.
        /// If you do this more than once, you need to resubscribe to events and re-set the hail object, etc.
        /// </summary>
        /// <param name="client"></param>
        public static void Initialize(Client client)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            if (Initialized)
            {
                Peer.Cleanup();
                Disconnect();
                Hook.Manager.Clear();
            }
            Initialized = true;

            Peer = client;
            OnDisconnectedFromServer += OnOnDisconnectedFromServer;
            UnityExtensions.AddSerializers(Peer.Serializer);
            Hook = EngineHook.Create(Peer);
        }

        private static void OnOnDisconnectedFromServer()
        {
            Hook.Manager.Clear();
        }

        internal static void CleanupEvents()
        {
            OnDisconnectedFromServer -= OnOnDisconnectedFromServer;
            Peer.Cleanup();
            Peer = null;
        }

        /// <summary>
        /// The method to use when converting from the pnets euler rotation to a quaternion
        /// </summary>
        public static Func<PNet.Structs.Vector3F, Quaternion> ConvertRotation;

        #region PNetC.Net instance -> PNetU.Net static bindings

        /// <summary>
        /// When the room is changing
        /// </summary>
        public static event Action<string> OnRoomChange
        {
            add { Peer.BeginRoomSwitch += value; }
            remove { Peer.BeginRoomSwitch -= value; }
        }

        /// <summary>
        /// When disconnected from the server
        /// </summary>
        public static event Action OnDisconnectedFromServer
        {
            add { Peer.OnDisconnectedFromServer += value; }
            remove { Peer.OnDisconnectedFromServer -= value; }
        }

        /// <summary>
        /// When disconnected from the room. Some of the reasons are specified in PNet.DtoPMsgs and the like.
        /// This is not fired when just doing a standard room switch.
        /// </summary>
        public static event Action<string> OnDisconnectedFromRoom
        {
            add { Peer.OnDisconnectedFromRoom += value; }
            remove { Peer.OnDisconnectedFromRoom -= value; }
        }

        /// <summary>
        /// When finished connecting to the server
        /// </summary>
        public static event Action OnConnectedToServer
        {
            add { Peer.OnConnectedToServer += value; }
            remove { Peer.OnConnectedToServer -= value; }
        }

        public static SerializationManager Serializer
        {
            get { return Peer.Serializer; }
        }

        /// <summary>
        /// The function to use for writing the connect data (username/password/etc)
        /// </summary>
        public static object HailMessage
        {
            get { return Peer.HailObject; }
            set { Peer.HailObject = value; }
        }

        public static void SubscribeToServer(byte rpcId, Action<NetMessage> processor)
        {
            Peer.Server.SubscribeToRpc(rpcId, processor);
        }

        public static void UnsubscribeFromServer(byte rpcId)
        {
            Peer.Server.UnsubscribeRpc(rpcId);
        }

        public static void SubscribeToRoom(byte rpcId, Action<NetMessage> processor)
        {
            Peer.Room.SubscribeToRpc(rpcId, processor);
        }

        public static void UnsubscribeFromRoom(byte rpcId)
        {
            Peer.Room.UnsubscribeRpc(rpcId);
        }

        /// <summary>
        /// When we've failed to connect
        /// </summary>
        public static event Action<string> OnFailedToConnect
        {
            add { Peer.OnFailedToConnect += value; }
            remove { Peer.OnFailedToConnect -= value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public static NetworkLogLevel LogLevel { get; set; } = NetworkLogLevel.Off;

        /// <summary>
        /// latest status
        /// </summary>
        public static ConnectionStatus Status { get { return Peer.Server.Status; } }

        /// <summary>
        /// reason for the most latest status
        /// </summary>
        public static string StatusReason { get { return Peer.Server.StatusReason; } }

        /// <summary>
        /// The Network ID of this client
        /// </summary>
        public static ushort PlayerId { get { return Peer.PlayerId; } }

        /// <summary>
        /// last received latency value from the lidgren's calculations
        /// </summary>
        public static float Latency { get { return Peer.Room.Latency; } }

        /// <summary>
        /// Network time of this frame, or last, depending on script order execution
        /// to make sure its this frame, set the EngineHook to execute first
        /// </summary>
        public static double Time { get { return Peer.Time; } }

        /// <summary>
        /// Connect with the specified configuration
        /// </summary>
        /// <param name="configuration"></param>
        public static void Connect(NetworkConfiguration configuration)
        {
            Peer.StartConnection(configuration);
        }

        /// <summary>
        /// Disconnect if connected
        /// </summary>
        public static void Disconnect(string reason = "disconnecting")
        {
            Peer.Shutdown(reason);
        }

        /// <summary>
        /// Send an rpc to the server
        /// </summary>
        /// <param name="rpcId"></param>
        /// <param name="args"></param>
        public static void RpcToServer(byte rpcId, params object[] args)
        {
            Peer.Server.Rpc(rpcId, args);
        }

        public static void RpcToRoom(byte rpcId, params object[] args)
        {
            Peer.Room.Rpc(rpcId, args);
        }

        /// <summary>
        /// Run this when the room changing has completed (tells the server you're actually ready to be in a room)
        /// </summary>
        public static void FinishedRoomChange()
        {
            Peer.FinishRoomSwitch();
        }
        #endregion
    }
}
