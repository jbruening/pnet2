﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
#if LIDGREN
using Lidgren.Network;
#endif

namespace PNet
{
    public static class Utilities
    {
        public static bool TryParseGuid(string str, out Guid guid)
        {
            if (str.Length != 32)
            {
                guid = new Guid();
                return false;
            }

            if (!Regex.IsMatch(str, "^[A-Fa-f0-9]{32}$|"))
            {
                guid = new Guid();
                return false;
            }

            try
            {
                guid = new Guid(str);
                return true;
            }
            catch (Exception e)
            {
                guid = new Guid();
                return false;
            }
        }

        public static T[] RemoveAt<T>(this T[] source, int index)
        {
            T[] dest = new T[source.Length - 1];
            if (index > 0)
                Array.Copy(source, 0, dest, 0, index);

            if (index < source.Length - 1)
                Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

            return dest;
        }

#if LIDGREN
        /// <summary>
        /// the current network time
        /// </summary>
        public static double Now { get { return NetTime.Now; } }
#else
        public static double Now { get { throw new NotImplementedException(); } }
#endif
    }

    public static class EventUtils
    {
        /// <summary>
        /// run the specified eventHandler if it is not null.
        /// Extension methods allow running methods on null objects
        /// </summary>
        /// <param name="eventHandler"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void Raise(this EventHandler eventHandler,
            object sender, EventArgs e)
        {
            if (eventHandler != null)
            {
                eventHandler(sender, e);
            }
        }

        /// <summary>
        /// run the specified eventHandler if it is not null.
        /// Extension methods allow running methods on null objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventHandler"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void Raise<T>(this EventHandler<T> eventHandler,
            object sender, T e) where T : EventArgs
        {
            if (eventHandler != null)
            {
                eventHandler(sender, e);
            }
        }

        /// <summary>
        /// run the specified eventHandler if it is not null.
        /// Extension methods allow running methods on null objects
        /// </summary>
        /// <param name="eventHandler"></param>
        public static void Raise(this Action eventHandler)
        {
            if (eventHandler != null)
                eventHandler();
        }
        public static void TryRaise(this Action eventHandler, ILogger logger)
        {
            try
            {
                if (eventHandler != null)
                    eventHandler();
            }
            catch (Exception e)
            {
                if (logger != null)
                    logger.Exception(e, "");
            }
        }

        /// <summary>
        /// run the specified eventHandler if it is not null.
        /// Extension methods allow running methods on null objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventHandler"></param>
        /// <param name="arg"></param>
        public static void Raise<T>(this Action<T> eventHandler, T arg)
        {
            if (eventHandler != null)
                eventHandler(arg);
        }

        public static void TryRaise<T>(this Action<T> eventHandler, T arg, ILogger logger)
        {
            try
            {
                if (eventHandler != null)
                    eventHandler(arg);
            }
            catch (Exception e)
            {
                if (logger != null)
                    logger.Exception(e, "");
            }
        }

        /// <summary>
        /// run the specified eventHandler if it is not null.
        /// Extension methods allow running methods on null objects
        /// </summary>
        /// <param name="eventHandler"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        public static void Raise<T1, T2>(this Action<T1, T2> eventHandler, T1 arg1, T2 arg2)
        {
            if (eventHandler != null)
                eventHandler(arg1, arg2);
        }
    }

    public static class AttributeExtensions
    {
        /// <summary>Searches and returns attributes. The inheritance chain is not used to find the attributes.</summary>
        /// <typeparam name="T">The type of attribute to search for.</typeparam>
        /// <param name="type">The type which is searched for the attributes.</param>
        /// <returns>Returns all attributes.</returns>
        public static T[] GetCustomAttributes<T>(this Type type) where T : Attribute
        {
            return GetCustomAttributes<T>(type, false).Select(arg => (T)arg).ToArray();
        }

        /// <summary>Searches and returns attributes.</summary>
        /// <typeparam name="T">The type of attribute to search for.</typeparam>
        /// <param name="type">The type which is searched for the attributes.</param>
        /// <param name="inherit">Specifies whether to search this member's inheritance chain to find the attributes. Interfaces will be searched, too.</param>
        /// <returns>Returns all attributes.</returns>
        public static T[] GetCustomAttributes<T>(this Type type, bool inherit) where T : Attribute
        {
            if (!inherit)
            {
                return CastAs<T>(type.GetCustomAttributes(typeof(T), false));
            }

            var attributeCollection = new List<T>();
            var baseType = type;

            do
            {
                baseType.GetCustomAttributes(typeof(T), true).Apply(a => attributeCollection.Add((T)a));
                baseType = baseType.BaseType;
            }
            while (baseType != null);

            foreach (var interfaceType in type.GetInterfaces())
            {
                GetCustomAttributes<T>(interfaceType, true).Apply(attributeCollection.Add);
            }

            var attributeArray = new T[attributeCollection.Count];
            attributeCollection.CopyTo(attributeArray, 0);
            return attributeArray;
        }

        private static T[] CastAs<T>(object[] objs)
        {
            var arr = Array.CreateInstance(typeof(T), objs.Length);
            Array.Copy(objs, arr, objs.Length);
            return (T[])arr;
        }

        /// <summary>Applies a function to every element of the list.</summary>
        private static void Apply<T>(this IEnumerable<T> enumerable, Action<T> function)
        {
            foreach (var item in enumerable)
            {
                function.Invoke(item);
            }
        }
    }
}
