﻿extern alias pnet;
using System;
using System.Collections.Generic;
using System.Linq;
using PNet.Structs;
using PNetR;
using UnityEngine;
using PNet;
using System.Collections;
using System.Reflection;
using Debug = UnityEngine.Debug;
using NetworkStateSynchronization = PNet.NetworkStateSynchronization;

namespace PNet2RoomUnity
{
    /// <summary>
    /// network synchronization
    /// </summary>
    [AddComponentMenu("PNet/Network View")]
    public class NetworkView : MonoBehaviour
    {
        private bool _stateDestroyed;
        private NetworkViewId _oldId;
        private const float StreamTime = 1 / 12f;
        private float _lastStream;

        private readonly HashSet<Player> _visPlayers = new HashSet<Player>();

        private Net _net;
        private PNetR.NetworkView _networkView;
        
        public PNetR.NetworkView NetView { get { return _networkView; } }

        public Player Owner { get { return NetView == null ? null : NetView.Owner; } }

        public NetworkStateSynchronization StateSynchronization { get; set; }

        public bool DestroyWhenNotVisible { get; set; }
        public bool NetworkDestroyed { get; private set; }

        public event Action<NetworkViewId> Destroyed;
        public event Action Stream;

        internal void SetNetworkView(Net net, PNetR.NetworkView netView)
        {
            _net = net;
            _networkView = netView;

            _networkView.GettingRotation += ViewOnGettingRotation;
            _networkView.GettingPosition += ViewOnGettingPosition;
            _networkView.Destroyed += ViewOnDestroyed;

            var components = gameObject.GetComponents<MonoBehaviour>();

            foreach (var component in components)
            {
                if (component == null)
                {
                    Debug.LogWarning("null monobehaviour? ", this);
                    continue;
                }
                byte id;
                if (component.GetType().GetNetId(out id))
                    SubscribeMarkedRpcsOnComponent(component);
            }
        }

        void LateUpdate()
        {
            if (StateSynchronization != NetworkStateSynchronization.Unreliable) return;
            if (Time.time - _lastStream < StreamTime) return;

            _lastStream = Time.time;
            Stream.Raise();
        }

        private Vector3F ViewOnGettingRotation()
        {
            return transform.rotation.eulerAngles.ToPNet();
        }

        private Vector3F ViewOnGettingPosition()
        {
            return transform.position.ToPNet();
        }

        private void ViewOnDestroyed()
        {
            try
            {
                if (!_stateDestroyed)
                    Destroy(gameObject);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ViewOnDestroyed failure for {0}: {1}", name, ex));
            }
            _networkView = null;
            NetworkDestroyed = true;
        }

        public void NetworkDestroy(byte reasonCode = 0)
        {
            if (NetView == null)
                throw new Exception(string.Format("Attempted to NetworkDestroy {0} when it had no View", name));

            var result = _networkView.Room.Destroy(_networkView, reasonCode);
            if (!result)
            {
                PNetR.Debug.LogError("Could not network destroy {0}. This will probably leak network views", name);
            }
        }

        internal bool VisCheck(Player player)
        {
            if (NetworkDestroyed) //something wrong here.
            //cannot use View to do checking, as we actually do a vischeck BEFORE assigning the View (during instantiation).
            {
                PNetR.Debug.LogError("Attempted to do vischeck on {0} object while it is destroying", name);
                return false;
            }
            if (_stateDestroyed)
            {
                PNetR.Debug.LogError("Attempted to do a vischeck on {0} object while it is state destroying", name);
            }

            return _visPlayers.Count == 0 || _visPlayers.Contains(player);
        }

        public Player[] PlayersVisibleTo
        {
            get { return _visPlayers.ToArray(); }
            set
            {
                _visPlayers.Clear();
                if (value == null) return;
                _visPlayers.UnionWith(value);
            }
        }

        internal void OnPlayerAdded(Player player)
        {

        }

        internal void OnPlayerRemoved(Player player)
        {
            _visPlayers.Remove(player);
            if (_visPlayers.Count == 0 && DestroyWhenNotVisible)
            {
                NetworkDestroy();
            }
        }

        /// <summary>
        /// Send an rpc
        /// </summary>
        /// <param name="rpcID"></param>
        /// <param name="mode"></param>
        /// <param name="args"></param>
        public void Rpc<T>(byte rpcID, RpcMode mode, params object[] args) where T : class
        {
            _networkView.Rpc<T>(rpcID, mode, args);
        }

        /// <summary>
        /// Send an rpc to the owner of this object
        /// </summary>
        /// <param name="rpcID"></param>
        /// <param name="args"></param>
        public void RpcToOwner<T>(byte rpcID, params object[] args) where T : class
        {
            _networkView.Rpc<T>(rpcID, RpcMode.OwnerOrdered, args);
        }

        #region rpc subscription
        /// <summary>
        /// Subscribe all the marked rpcs on the supplied component
        /// </summary>
        /// <param name="behaviour"></param>
        public void SubscribeMarkedRpcsOnComponent(MonoBehaviour behaviour)
        {
            _networkView.SubscribeMarkedRpcsOnComponent(behaviour);
        }
        #endregion

        #region proxy
        /// <summary>
        /// Add a proxy to the network view to use for Proxy`T()
        /// </summary>
        /// <param name="proxy"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public void AddProxy(INetComponentProxy proxy)
        {
            _networkView.AddProxy(proxy);
        }

        public void RemoveProxy(INetComponentProxy proxy)
        {
            _networkView.RemoveProxy(proxy);
        }

        public void RemoveProxy<T>()
        {
            _networkView.RemoveProxy<T>();
        }

        public T Proxy<T>()
        {
            return NetView == null ? default(T) : NetView.Proxy<T>();
        }

        public T Proxy<T>(Player player)
        {
            return NetView == null ? default(T) : NetView.Proxy<T>(player);
        }

        public T Proxy<T>(RpcMode mode)
        {
            return NetView == null ? default(T) : NetView.Proxy<T>(mode);
        }
        #endregion

        
        private void OnDestroy()
        {
            Destroyed.Raise(_oldId);
            Destroyed = null;
            _stateDestroyed = true;

            if (NetView != null && !NetworkDestroyed)
                if (!NetView.Room.Destroy(NetView))
                {
                    PNetR.Debug.LogError(
                        "Could not network destroy {0} during OnDestroy. This will probably leak network views", NetView.Id);
                }

            CleanupNetView();
        }

        void CleanupNetView()
        {
            if (_networkView != null) //this will get run usually if we're switching scenes
            {
                _net.Manager.Remove(_networkView);

                _networkView.Destroyed -= ViewOnDestroyed;
                _networkView.ClearProxies();
                _networkView.ClearSubscriptions();
                _networkView = null;
            }
        }
    }
}
