﻿using System;
using System.Collections.Generic;
using PNet;

namespace PNetR
{
    public abstract class ARoomServer
    {
        protected internal Room Room;

        protected internal abstract void Setup();
        protected internal abstract void Start();
        protected internal abstract void ReadQueue();
        protected internal abstract void Shutdown(string reason);

        protected internal abstract NetMessage GetMessage(int size);

        protected void AddingPlayer(Player player)
        {
            Room.AddPlayer(player);
        }

        protected void RemovePlayer(Player player)
        {
            Room.RemovePlayer(player);
        }

        protected Player ConstructNewPlayer(APlayerConnection connection)
        {
            var player = Room.ConstructNewPlayer(connection);
            return player;
        }

        protected void VerifyPlayerConnecting(Player player, Guid token)
        {
            Room.VerifyPlayerConnecting(player, token);
        }

        protected void VerifyWaitingPlayers()
        {
            Room.VerifyWaitingPlayers();
        }

        protected internal abstract void SendToPlayers(NetMessage msg, ReliabilityMode mode);
        protected internal abstract void SendToPlayers(List<Player> players, NetMessage msg, ReliabilityMode mode);
        protected internal abstract void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode);
        protected internal abstract void SendExcept(NetMessage msg, Player except, ReliabilityMode mode);
        protected internal abstract void SendSceneView(NetMessage msg, ReliabilityMode mode, List<Player> players);
        
        protected internal abstract void AllowConnect(Player player);
        protected internal abstract void Disconnect(Player player, string reason);

        protected void ConsumeData(Player player, NetMessage msg)
        {
            player.ConsumeData(msg);
        }
    }

    public abstract class APlayerConnection
    {
        public ARoomServer Server { get; }

        protected APlayerConnection(ARoomServer server)
        {
            Server = server;
        }
    }

    public class NullPlayerConnection : APlayerConnection
    {
        class NullRoomServer : ARoomServer
        {
            protected internal override void AllowConnect(Player player)
            {
            }

            protected internal override void Disconnect(Player player, string reason)
            {
            }

            protected internal override NetMessage GetMessage(int size)
            {
                return NetMessage.GetMessage(size);
            }

            protected internal override void ReadQueue()
            {
            }

            protected internal override void SendExcept(NetMessage msg, Player except, ReliabilityMode mode)
            {
                NetMessage.RecycleMessage(msg);
            }

            protected internal override void SendSceneView(NetMessage msg, ReliabilityMode mode, List<Player> players)
            {
                NetMessage.RecycleMessage(msg);
            }

            protected internal override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode)
            {
                NetMessage.RecycleMessage(msg);
            }

            protected internal override void SendToPlayers(NetMessage msg, ReliabilityMode mode)
            {
                NetMessage.RecycleMessage(msg);
            }

            protected internal override void SendToPlayers(List<Player> players, NetMessage msg, ReliabilityMode mode)
            {
                NetMessage.RecycleMessage(msg);
            }

            protected internal override void Setup()
            {
            }

            protected internal override void Shutdown(string reason)
            {
            }

            protected internal override void Start()
            {
            }
        }

        private NullPlayerConnection() : base(new NullRoomServer())
        {
        }

        /// <summary>
        /// Represents a player that all messages get sent to nothing.
        /// </summary>
        public static NullPlayerConnection Value { get; } = new NullPlayerConnection();
    }
}
