﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using PNet;

namespace PNetR.Impl
{
    public class MixedRoomServer : ARoomServer
    {
        const int MaxUdpSize = 1500;

        class Client
        {
            public IPAddress Address;
            public TcpClient Tcp;
            public IPEndPoint UdpEp;
            public readonly byte[] UdpMsg = new byte[MaxUdpSize];
            public int UdpLength;
            public bool FinishedConnect;
        }
        
        private UdpClient _udpServer;
        private TcpListener _tcpServer;

        private bool _acceptEnabled;
        private bool _tcpRunning;

        private Thread _udpThread;

        /// <summary>
        /// udp clients always use the same port that their tcp connection is over.
        /// </summary>
        private Dictionary<IPEndPoint, Client> _udpClients;
        private readonly Queue<Action> _userThreadQueue;
        private readonly Queue<NetMessage> _msgQueue;

        protected internal override void Setup()
        {
            //yes, ctor
            _udpServer = new UdpClient(Room.Configuration.ListenPort);
            _tcpServer = new TcpListener(IPAddress.Any, Room.Configuration.ListenPort);

            _acceptEnabled = true;
            _tcpRunning = true;
        }

        protected internal override void Start()
        {
            _udpThread = new Thread(UdpLoop) { IsBackground = true, Name = "PNet2 Udp Reader" };
            _udpThread.Start();

            _tcpServer.Start();
            _tcpServer.BeginAcceptTcpClient(EndAcceptTcpClient, null);
        }

        #region udp
        private void UdpLoop()
        {
            //todo: async receive messages and copy them into the client's udp message.
            var buffer = new byte[MaxUdpSize];

            while (_tcpRunning)
            {
                EndPoint sep = new IPEndPoint(IPAddress.Any, 0);
                var length = _udpServer.Client.ReceiveFrom(buffer, ref sep);
                var senderEp = sep as IPEndPoint;

                Client client;
                lock(_udpClients)
                    _udpClients.TryGetValue(senderEp, out client);

                if (client != null)
                {
                    lock (client.UdpMsg)
                    {
                        Array.Copy(buffer, client.UdpMsg, length);
                        client.UdpLength = length;
                    }
                }
            }
        }
        #endregion

        #region tcp
        private void EndAcceptTcpClient(IAsyncResult ar)
        {
            var client = _tcpServer.EndAcceptTcpClient(ar);
            var thread = new Thread(HandleTcpClient) { IsBackground = true, Name = "PNet2 Tcp Client" };
            thread.Start(client);

            if (_acceptEnabled)
                _tcpServer.BeginAcceptTcpClient(EndAcceptTcpClient, null);
        }

        private void HandleTcpClient(object state)
        {
            var thread = Thread.CurrentThread;
            var client = state as TcpClient;
            client.ReceiveBufferSize = 1024;
            client.ReceiveTimeout = 5000;
            client.SendBufferSize = 1024;
            client.NoDelay = true;
            client.LingerState = new LingerOption(true, 10);
            var clientEp = client.Client.RemoteEndPoint as IPEndPoint;

            try
            {
                using (var networkStream = client.GetStream())
                {
                    //client now starts auth
                    byte[] buffer = new byte[1024];
                    NetMessage dataBuffer = null;
                    var bufferSize = 0;
                    var lengthBuffer = new byte[2];
                    var bytesReceived = 0;
                    int readBytes;

                    var authMessages = new Queue<NetMessage>();
                    while (_acceptEnabled)
                    {
                        readBytes = networkStream.Read(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            var readMessage = NetMessage.GetMessages(buffer, readBytes, ref bytesReceived,
                                ref dataBuffer, ref lengthBuffer, ref bufferSize, authMessages.Enqueue);
                            if (readMessage >= 1)
                                break;
                        }
                        if (!client.Connected)
                            return;
                    }

                    if (Room.PlayerCount >= Room.Configuration.MaximumPlayers)
                    {
                        var msg = GetMessage(DtoPMsgs.NoRoom.Length * 2 + 1);
                        msg.Write(false);
                        msg.WritePadBits();
                        msg.Write(DtoPMsgs.NoRoom);
                        WriteAsync(networkStream, msg);
                        return;
                    }
                    var authMessage = authMessages.Dequeue();

                    var sclient = new Client { Address = clientEp.Address, UdpEp = clientEp, Tcp = client };
                    var player = ConstructNewPlayer(sclient);
                    Guid token;
                    if (authMessage.ReadGuid(out token))
                    {
                        OnUserThread(() => VerifyPlayerConnecting(player, token));
                    }
                    else
                    {
                        var msg = GetMessage(DtoPMsgs.BadToken.Length * 2 + 1);
                        msg.Write(false);
                        msg.WritePadBits();
                        msg.Write(DtoPMsgs.BadToken);
                        WriteAsync(networkStream, msg);
                        return;
                    }


                    while (authMessages.Count > 0)
                        AddTcpMessage(authMessages.Dequeue());

                    while (_tcpRunning)
                    {
                        readBytes = networkStream.Read(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            NetMessage.GetMessages(buffer, readBytes, ref bytesReceived, ref dataBuffer,
                                ref lengthBuffer, ref bufferSize, msg => { if (sclient.FinishedConnect) AddTcpMessage(msg); });
                        }
                        else
                            EnsureConnected(sclient);
                        if (!client.Connected)
                            return;
                    }
                }
            }
            finally
            {
                client.Close();
            }
        }

        private void AddTcpMessage(NetMessage msg)
        {
            lock (_msgQueue)
                _msgQueue.Enqueue(msg);
        }

        private void OnUserThread(Action action)
        {
            lock (_userThreadQueue)
                _userThreadQueue.Enqueue(action);
        }

        private void EnsureConnected(Client client)
        {
            var msg = GetMessage(2);
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            msg.Write(DandPRpcs.Ping);
            //SendTcpMessage(connection, msg);
        }

        private void WriteAsync(NetworkStream stream, NetMessage msg)
        {
            stream.BeginWrite(msg.Data, 0, msg.LengthBytes, EndStreamWrite, stream);
        }

        private void EndStreamWrite(IAsyncResult ar)
        {
            var stream = ar.AsyncState as NetworkStream;
            stream.EndWrite(ar);
        }
        #endregion

        protected internal override void AllowConnect(Player player)
        {
            var client = player.Connection as Client;
            client.FinishedConnect = true;
        }

        protected internal override void Disconnect(Player player, string reason)
        {
            throw new NotImplementedException();
        }

        protected internal override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessageSizePad(size);
        }

        protected internal override void ReadQueue()
        {
            throw new NotImplementedException();
        }

        protected internal override void SendExcept(NetMessage msg, Player except, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected internal override void SendSceneView(NetMessage msg, ReliabilityMode mode, List<Player> players)
        {
            throw new NotImplementedException();
        }

        protected internal override void SendToConnections(List<object> connections, NetMessage msg, ReliabilityMode reliable)
        {
            throw new NotImplementedException();
        }

        protected internal override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected internal override void SendToPlayers(NetMessage msg, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected internal override void SendToPlayers(List<Player> players, NetMessage msg, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected internal override void Shutdown(string reason)
        {
            throw new NotImplementedException();
        }
    }
}
