﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using PNet;

namespace PNetR.Impl
{
    public class TcpRoomServer : ARoomServer
    {
        class ConnectionInfo : APlayerConnection
        {
            public TcpClient Client { get; }
            public NetworkStream Stream { get; }
            public bool FinishedConnect;
            public Player Player;

            public ConnectionInfo(ARoomServer server, TcpClient client, NetworkStream networkStream) : base(server)
            {
                Client = client;
                Stream = networkStream;
            }
        }

        public class Configuration
        {
            public int? ListenPort { get; set; }
        }

        private TcpListener tcpListener;

        private bool acceptEnabled;
        private bool tcpRunning;
        private readonly List<ConnectionInfo> connections = new List<ConnectionInfo>();

        private readonly Queue<Action> userThreadQueue;
        private readonly Queue<NetMessage> msgQueue;

        private readonly Configuration configuration;

        public TcpRoomServer(Configuration configuration = null)
        {
            this.configuration = configuration ?? new Configuration();
        }

        protected internal override void Setup()
        {
            tcpListener = new TcpListener(IPAddress.Any, Room.Configuration.ListenPort);

            acceptEnabled = true;
            tcpRunning = true;
        }

        protected internal override void Start()
        {
            tcpListener.Start();
            tcpListener.BeginAcceptTcpClient(EndAcceptTcpClient, null);
        }

        private void EndAcceptTcpClient(IAsyncResult ar)
        {
            var client = tcpListener.EndAcceptTcpClient(ar);
            var thread = new Thread(HandleTcpClient) { IsBackground = true, Name = "PNet2 Tcp Client" };
            thread.Start(client);

            if (acceptEnabled)
                tcpListener.BeginAcceptTcpClient(EndAcceptTcpClient, null);
        }

        private void HandleTcpClient(object state)
        {
            var thread = Thread.CurrentThread;
            var client = state as TcpClient;
            client.ReceiveBufferSize = 1024;
            client.ReceiveTimeout = 5000;
            client.SendBufferSize = 1024;
            client.NoDelay = true;
            client.LingerState = new LingerOption(true, 10);
            var clientEp = client.Client.RemoteEndPoint as IPEndPoint;

            try
            {
                using (var networkStream = client.GetStream())
                {
                    //client now starts auth
                    byte[] buffer = new byte[ushort.MaxValue];
                    NetMessage dataBuffer = null;
                    var bufferSize = 0;
                    var lengthBuffer = new byte[2];
                    var bytesReceived = 0;
                    int readBytes;

                    var firstMessages = new Queue<NetMessage>();
                    while (acceptEnabled)
                    {
                        readBytes = networkStream.Read(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            var readMessage = NetMessage.GetMessages(buffer, readBytes, ref bytesReceived,
                                ref dataBuffer, ref lengthBuffer, ref bufferSize, firstMessages.Enqueue);
                            if (readMessage >= 1)
                                break;
                        }
                        if (!client.Connected)
                            return;
                    }

                    if (Room.PlayerCount >= Room.Configuration.MaximumPlayers)
                    {
                        var msg = GetMessage(DtoPMsgs.NoRoom.Length * 2 + 1);
                        msg.Write(false);
                        msg.WritePadBits();
                        msg.Write(DtoPMsgs.NoRoom);
                        WriteAsync(networkStream, msg);
                        return;
                    }
                    var authMessage = firstMessages.Dequeue();

                    var sclient = new ConnectionInfo(this, client, networkStream);
                    var player = ConstructNewPlayer(sclient);
                    Guid token;
                    if (authMessage.ReadGuid(out token))
                    {
                        OnUserThread(() => VerifyPlayerConnecting(player, token));
                    }
                    else
                    {
                        var msg = GetMessage(DtoPMsgs.BadToken.Length * 2 + 1);
                        msg.Write(false);
                        msg.WritePadBits();
                        msg.Write(DtoPMsgs.BadToken);
                        WriteAsync(networkStream, msg);
                        return;
                    }


                    while (firstMessages.Count > 0)
                        AddTcpMessage(firstMessages.Dequeue());

                    while (tcpRunning)
                    {
                        readBytes = networkStream.Read(buffer, 0, buffer.Length);
                        if (readBytes > 0)
                        {
                            NetMessage.GetMessages(buffer, readBytes, ref bytesReceived, ref dataBuffer,
                                ref lengthBuffer, ref bufferSize, msg => { if (sclient.FinishedConnect) AddTcpMessage(msg); });
                        }
                        else
                            EnsureConnected(sclient);
                        if (!client.Connected)
                            return;
                    }
                }
            }
            finally
            {
                client.Close();
            }
        }

        private void AddTcpMessage(NetMessage msg)
        {
            lock(msgQueue)
                msgQueue.Enqueue(msg);
        }

        private void OnUserThread(Action action)
        {
            lock (userThreadQueue)
                userThreadQueue.Enqueue(action);
        }

        private void EnsureConnected(ConnectionInfo client)
        {
            var msg = GetMessage(2);
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            msg.Write(DandPRpcs.Ping);
            WriteAsync(client.Stream, msg);
        }

        private void SendTcpMessage(Player player, NetMessage msg)
        {
            WriteAsync(GetStream(player), msg);
        }

        class AsyncWriteInfo
        {
            public AsyncWriteInfo(NetworkStream stream, NetMessage msg)
            {
                Stream = stream;
                Message = msg;
            }

            public NetworkStream Stream { get; }
            public NetMessage Message { get; }
        }

        private void WriteAsync(NetworkStream stream, NetMessage msg)
        {
            stream.BeginWrite(msg.Data, 0, msg.LengthBytes, EndStreamWrite, new AsyncWriteInfo(stream, msg));
        }

        private void EndStreamWrite(IAsyncResult ar)
        {
            var awi = ar.AsyncState as AsyncWriteInfo;
            try
            {
                awi.Stream.EndRead(ar);
            }
            finally
            {
                NetMessage.RecycleMessage(awi.Message);
            }
        }

        protected internal override void AllowConnect(Player player)
        {
            var msg = GetMessage(17);
            msg.Write(true);
            msg.WritePadBits();
            msg.Write(Room.RoomId);
            msg.WriteSize();
            SendTcpMessage(player, msg);
        }

        protected internal override void Disconnect(Player player, string reason)
        {
            var msg = GetMessage(reason.Length * 2 + 5);
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            msg.Write(RandPRpcs.DisconnectMessage);
            msg.Write(reason);
            SendTcpMessage(player, msg);            
            (player.Connection as ConnectionInfo).Client.Close();
        }

        protected internal override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessageSizePad(size);
        }

        protected internal override void ReadQueue()
        {
            Action[] actions;
            lock (userThreadQueue)
            {
                actions = userThreadQueue.ToArray();
                userThreadQueue.Clear();
            }

            foreach (var action in actions)
            {
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

            NetMessage[] messages;
            lock (msgQueue)
            {
                messages = msgQueue.ToArray();
                msgQueue.Clear();
            }

            foreach(var msg in messages)
            {

            }




            VerifyWaitingPlayers();
        }

        protected internal override void SendExcept(NetMessage msg, Player except, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected internal override void SendSceneView(NetMessage msg, ReliabilityMode mode, List<Player> players)
        {
            throw new NotImplementedException();
        }

        protected internal override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected internal override void SendToPlayers(NetMessage msg, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected internal override void SendToPlayers(List<Player> players, NetMessage msg, ReliabilityMode mode)
        {
            throw new NotImplementedException();
        }

        protected internal override void Shutdown(string reason)
        {
            throw new NotImplementedException();
        }

        private NetworkStream GetStream(Player player)
        {
            return (player.Connection as ConnectionInfo).Stream;
        }
    }
}
