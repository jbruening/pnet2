﻿using System;
using System.Collections.Generic;
using Lidgren.Network;
using PNet;

namespace PNetR.Impl
{
    public class LidgrenRoomServer : ARoomServer
    {
        class ConnectionInfo : APlayerConnection
        {
            public NetConnection Connection { get; }

            public ConnectionInfo(ARoomServer server, NetConnection connection)
                : base(server)
            {
                Connection = connection;
            }
        }

        private NetServer _playerServer;
        private NetPeerConfiguration _serverConfiguration;

        private int _lastRoomFrameSize = 16;

        protected internal override void Setup()
        {
            _serverConfiguration = new NetPeerConfiguration(Room.Configuration.AppIdentifier);
            _serverConfiguration.SetMessageTypeEnabled(NetIncomingMessageType.ConnectionApproval, true);
            _serverConfiguration.Port = Room.Configuration.ListenPort;
            //bit of overhead in connection count
            _serverConfiguration.MaximumConnections = (int)(Room.Configuration.MaximumPlayers * 1.5);

#if DEBUG
            Debug.Log("Debug build. Simulated latency and packet loss/duplication is enabled.");
            _serverConfiguration.SimulatedLoss = 0.001f;
            _serverConfiguration.SimulatedDuplicatesChance = 0.001f;
            _serverConfiguration.SimulatedMinimumLatency = 0.1f;
            _serverConfiguration.SimulatedRandomLatency = 0.01f;
#endif

            _playerServer = new NetServer(_serverConfiguration);
        }

        protected internal override void Start()
        {
            if (_playerServer.Status != NetPeerStatus.NotRunning) return;
            _playerServer.Start();
        }

        protected internal override void Shutdown(string reason)
        {
            _playerServer.Shutdown(reason);
        }

        protected internal override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessage(size);
        }

        protected internal override void ReadQueue()
        {
            if (_playerServer == null) return;

            var messages = new List<NetIncomingMessage>(_lastRoomFrameSize * 2);
            _lastRoomFrameSize = _playerServer.ReadMessages(messages);

            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < messages.Count; i++)
            {
                var msg = NetMessage.GetMessage(messages[i].Data.Length);
                var msgType = messages[i].MessageType;
                var method = messages[i].DeliveryMethod;
                var senderConnection = messages[i].SenderConnection;
                messages[i].Clone(msg);
                msg.Sender = senderConnection;
                _playerServer.Recycle(messages[i]);

                if (msgType == NetIncomingMessageType.Data)
                {
                    ConsumeData(GetPlayer(senderConnection), msg);
                }
                else if (msgType == NetIncomingMessageType.DebugMessage)
                {
                    Debug.Log(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.WarningMessage)
                {
                    Debug.LogWarning(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.ConnectionLatencyUpdated)
                {
                    var latency = msg.ReadFloat();
                    //todo: do something with this latency.
                }
                else if (msgType == NetIncomingMessageType.ErrorMessage)
                {
                    Debug.LogError(msg.ReadString());
                }
                else if (msgType == NetIncomingMessageType.ConnectionApproval)
                {
                    var player = ConstructNewPlayer(new ConnectionInfo(this, senderConnection));
                    senderConnection.Tag = player;
                    if (Room.PlayerCount > Room.Configuration.MaximumPlayers)
                    {
                        senderConnection.Deny(DtoPMsgs.NoRoom);
                    }
                    else
                    {
                        Guid token;
                        if (msg.ReadGuid(out token))
                        {
                            VerifyPlayerConnecting(player, token);
                        }
                        else
                        {
                            senderConnection.Deny(DtoPMsgs.BadToken);
                        }
                    }
                }
                else if (msgType == NetIncomingMessageType.StatusChanged)
                {
                    var status = (NetConnectionStatus)msg.ReadByte();
                    var statusReason = msg.ReadString();
                    Debug.Log("ServerStatus for {2}: {0}, {1}", status, statusReason, senderConnection);

                    if (status == NetConnectionStatus.Connected)
                    {
                        AddingPlayer(GetPlayer(senderConnection));
                    }
                    else if (status == NetConnectionStatus.Disconnected)
                    {
                        RemovePlayer(GetPlayer(senderConnection));
                    }
                }
                else if (msgType == NetIncomingMessageType.Error)
                {
                    Debug.LogException(new Exception(msg.ReadString()) { Source = "Room.Lidgren.IplementationQueueRead [Lidgren PlayerServer Error]" }); //this should really never happen...
                }
                else
                {
                    Debug.LogWarning("Uknown message type {0} from player {1}", msgType, senderConnection);
                }

                NetMessage.RecycleMessage(msg);
            }

            VerifyWaitingPlayers();
        }

        private Player GetPlayer(NetConnection netConnection)
        {
            return netConnection.Tag as Player;
        }

        protected internal override void SendToPlayers(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = _playerServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            var method = mode.RoomDelivery();
            _playerServer.SendToAll(lmsg, method);
        }

        protected internal override void SendToPlayers(List<Player> players, NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = _playerServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);
            var conns = new List<NetConnection>(players.Count);
// ReSharper disable once ForCanBeConvertedToForeach speed is necessary
            for(int i = 0; i < players.Count; i++)
            {
                if (players[i] == null) continue;
                conns.Add(GetLConnection(players[i]));
            }
            if (conns.Count == 0) return;
            _playerServer.SendMessage(lmsg, conns, method, seq);
        }

        protected internal override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = _playerServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);
            _playerServer.SendMessage(lmsg, GetLConnection(player), method, seq);
        }

        protected internal override void SendExcept(NetMessage msg, Player except, ReliabilityMode mode)
        {
            var lmsg = _playerServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);
            _playerServer.SendToAll(lmsg, GetLConnection(except), method, seq);
        }

        protected internal override void SendSceneView(NetMessage msg, ReliabilityMode mode, List<Player> players)
        {
            var lmsg = _playerServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);
            if (seq == 2) seq = 3; //don't use player channel...

            if (players == null)
                _playerServer.SendToAll(lmsg, null, method, seq);
            else
            {
                var conns = new List<NetConnection>(players.Count);
                for (int i = 0; i < players.Count; i++)
                {
                    if (players[i] == null) continue;
                    conns.Add(GetLConnection(players[i]));
                }

                if (conns.Count > 0)
                    _playerServer.SendMessage(lmsg, conns, method, seq);
            }
        }

        protected internal override void AllowConnect(Player player)
        {
            var msg = _playerServer.CreateMessage(16);
            msg.Write(Room.RoomId.ToByteArray());
            GetLConnection(player).Approve(msg);
        }

        protected internal override void Disconnect(Player player, string reason)
        {
            GetLConnection(player).Disconnect(reason);
        }

        private NetConnection GetLConnection(Player player)
        {
            return (player.Connection as ConnectionInfo).Connection;
        }
    }
}
