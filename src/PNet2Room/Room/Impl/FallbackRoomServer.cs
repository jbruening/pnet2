﻿using System;
using System.Collections.Generic;
using System.Linq;
using PNet;

namespace PNetR.Impl
{
    public class FallbackRoomServer : ARoomServer
    {
        private readonly ARoomServer primary;
        private readonly ARoomServer backup;

        public FallbackRoomServer(ARoomServer primary, ARoomServer backup)
        {
            this.primary = primary;
            this.backup = backup;
        }

        protected internal override void AllowConnect(Player player)
        {
            player.Connection.Server.AllowConnect(player);
        }

        protected internal override void Disconnect(Player player, string reason)
        {
            player.Connection.Server.Disconnect(player, reason);
        }

        protected internal override NetMessage GetMessage(int size)
        {
            return primary.GetMessage(size);
        }

        protected internal override void ReadQueue()
        {
            primary.ReadQueue();
            backup.ReadQueue();
        }

        protected internal override void SendExcept(NetMessage msg, Player except, ReliabilityMode mode)
        {
            primary.SendExcept(msg, except, mode);
        }

        protected internal override void SendSceneView(NetMessage msg, ReliabilityMode mode, List<Player> players)
        {
            SplitPlayers(players, out var primaries, out var backups);

            primary.SendSceneView(msg, mode, primaries);
            backup.SendSceneView(msg, mode, backups);
        }

        protected internal override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode)
        {
            player.Connection.Server.SendToPlayer(player, msg, mode);
        }

        protected internal override void SendToPlayers(NetMessage msg, ReliabilityMode mode)
        {
            primary.SendToPlayers(msg, mode);
        }

        protected internal override void SendToPlayers(List<Player> players, NetMessage msg, ReliabilityMode mode)
        {
            SplitPlayers(players, out var primaries, out var backups);

            primary.SendToPlayers(primaries, msg, mode);
            backup.SendToPlayers(backups, msg, mode);
        }

        private void SplitPlayers(List<Player> players, out List<Player> primaries, out List<Player> backups)
        {
            primaries = new List<Player>(players.Count);
            backups = new List<Player>(players.Count);
            for (var i = 0; i < players.Count; i++)
            {
                if (players[i]?.Connection.Server == primary)
                    primaries.Add(players[i]);
                else if (players[i]?.Connection.Server == backup)
                    backups.Add(players[i]);
            }
        }

        protected internal override void Setup()
        {
            primary.Setup();
            backup.Setup();
        }

        protected internal override void Shutdown(string reason)
        {
            backup.Shutdown(reason);
            primary.Shutdown(reason);
        }

        protected internal override void Start()
        {
            primary.Start();
            backup.Start();
        }
    }
}
