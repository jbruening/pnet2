﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PNet;

namespace PNetR
{
    public class NetworkViewManager
    {
        readonly IntDictionary<NetworkView> _networkViews = new IntDictionary<NetworkView>();

        /// <summary>
        /// an array of all network views. Null checks are required, as empty keys will be null.
        /// </summary>
        public NetworkView[] AllViews
        {
            get
            {
                lock (_networkViews)
                {
                    return _networkViews.Values;
                }
            }
        }

        /// <summary>
        /// For rpcs that are not handled by a subscribed method, either allow or disallow them to continue forwarding
        /// </summary>
        public bool AllowUnhandledRpcForwarding { get; set; }

        /// <summary>
        /// Event called when a message is about to be forwarded
        /// </summary>
        public Action<NetMessageInfo, NetMessage> WillForwardMessage;

        public readonly Room Room;

        internal NetworkViewManager(Room room)
        {
            Room = room;
        }

        internal NetworkView GetNew(Player owner)
        {
            lock (_networkViews)
            {
                var nid = _networkViews.Add(null);
                var view = new NetworkView(this, new NetworkViewId((ushort) nid), owner);
                _networkViews[nid] = view;
                return view;
            }
        }

        public NetworkView Get(NetworkViewId id)
        {
            NetworkView view;
            _networkViews.TryGetValue(id.Id, out view);
            return view;
        }

        /// <summary>
        /// Checks if the specified object is in the manager. Verifies by id and reference.
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        public bool Contains(NetworkView view)
        {
            NetworkView oview;
            if (_networkViews.TryGetValue(view.Id.Id, out oview))
                return oview == view;
            return false;
        }

        /// <summary>
        /// remove the network view. This does not verify that the object in the slot was the same thing. Use contains to check that.
        /// </summary>
        /// <param name="view"></param>
        internal void Remove(NetworkView view)
        {
            lock(_networkViews)
                _networkViews.Remove(view.Id.Id);
        }

        /// <summary>
        /// Remove the network view, and make sure it was the same one via reference equality
        /// </summary>
        /// <param name="view"></param>
        /// <throws>IndexOutOfRange</throws>
        internal void RemoveVerified(NetworkView view)
        {
            lock(_networkViews)
            {
                NetworkView existing;
                if (!_networkViews.TryGetValue(view.Id.Id, out existing))
                    throw new KeyNotFoundException("network view " + view.Id.Id + " does not exist");
                if (!ReferenceEquals(existing, view))
                    throw new InvalidOperationException("network view " + view.Id.Id + " attempted to be removed, but it is not referenced as that id");

                _networkViews.Remove(view.Id.Id);
            }
        }

        internal void CallRpc(NetMessage msg, NetMessageInfo info, SubMsgType sub)
        {
            if (msg.RemainingBits < 32)
            {
                Debug.LogWarning("Attempted to call an rpc on a network view, but there weren't enough bits remaining to do it.");
                return;
            }
            var id = msg.ReadUInt16();
            var comp = msg.ReadByte();
            var rpc = msg.ReadByte();

            NetworkView view;
            var supposedToHave = _networkViews.TryGetValue(id, out view);
            if (supposedToHave && view != null)
                view.IncomingRpc(comp, rpc, msg, info, sub);
            else
            {
                Debug.LogWarning("Could not find view {0} to call c {1} rpc {2}", id, comp, rpc);
            }

            //todo: filter if rpc mode is all/others/owner, and then send to appropriate people.
            if (info.ContinueForwarding && view != null && TryForwardMessage(info, msg))
            {
                if (info.Mode == BroadcastMode.Others)
                {
                    view.SendExcept(msg, info.Sender, info.Reliability);
                }
                else if (info.Mode == BroadcastMode.All)
                {
                    view.SendMessage(msg, RpcUtils.RpcMode(info.Reliability, info.Mode));
                }
                else if (info.Mode == BroadcastMode.Owner && info.Sender != view.Owner && view.Owner.IsValid)
                {
                    view.Owner.SendMessage(msg, info.Reliability);
                }
            }
        }

        public void Stream(NetMessage msg, Player sender)
        {
            if (msg.RemainingBits < 16)
            {
                Debug.LogWarning("Attempted to read stream for an rpc but there weren't enough bits to do it");
                return;
            }
            var id = msg.ReadUInt16();
            NetworkView view;
            if (_networkViews.TryGetValue(id, out view))
                view.IncomingStream(msg, sender);
            else
            {
                //Debug.LogWarning("Could not find view {0} to stream to", id);
            }
        }

        /// <summary>
        /// one last attempt at if we should forward the message
        /// </summary>
        /// <param name="info"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool TryForwardMessage(NetMessageInfo info, NetMessage msg)
        {
            try
            {
                if (WillForwardMessage != null)
                {
                    WillForwardMessage(info, msg);
                }

                return info.ContinueForwarding;
            }
            catch(Exception e)
            {
                Debug.LogError("Could not run WillForwardMessage. Canceling forwarding.");
                return false;
            }
        }

        public void FinishedInstantiate(Player player, NetMessage msg)
        {
            if (msg.RemainingBits < 16)
            {
                Debug.LogError("Malformed finished instantiate");
                return;
            }

            var id = msg.ReadUInt16();
            NetworkView view;
            if (_networkViews.TryGetValue(id, out view))
            {
                view.OnFinishedInstantiate(player);
            }
        }
    }
}