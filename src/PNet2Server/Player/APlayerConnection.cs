﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PNetS
{
    public abstract class APlayerConnection
    {
        public APlayerServer Server { get; }

        protected APlayerConnection(APlayerServer server)
        {
            Server = server;
        }
    }
}
