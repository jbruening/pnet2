﻿using PNet;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PNetS
{
    public abstract class APlayerServer
    {
        public virtual Server Server { get; internal set; }

        protected internal abstract void Initialize();
        protected internal abstract NetMessage GetMessage(int length);
        protected internal abstract Task Shutdown(string reason);

        protected internal abstract void AllowPlayerToConnect(Player player);
        protected internal abstract void Disconnect(Player player, string reason);

        protected internal abstract void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode, bool recycle);
        protected internal abstract void SendToAllPlayers(NetMessage msg, ReliabilityMode mode);
        protected internal abstract void SendToAllPlayersExcept(Player player, NetMessage msg, ReliabilityMode mode);

        protected void FinalizePlayerAdd(Player player)
        {
            Server.FinalizePlayerAdd(player);
        }

        protected void ConsumeData(Player player, NetMessage msg)
        {
            player.ConsumeData(msg);
        }

        protected void PlayerAttemptingConnection(APlayerConnection connection, IPEndPoint endpoint, Action<Player> ctor, NetMessage msg)
        {
            Server.PlayerConnecting(connection, endpoint, ctor, msg);
        }

        protected void RemovePlayer(Player player)
        {
            Server.RemovePlayer(player);
        }

#if DEBUG
        protected void RemovePlayerNoNotify(Player player)
        {
            Server.RemovePlayerNoNotify(player);
        }
#endif
    }
}
