﻿using System;
using PNet;

namespace PNetS
{
    public partial class Player
    {
        /// <summary>
        /// Send the args as an rpc.
        /// serialization resolution is as follows:
        /// INetSerializable, Server.Serializers, Internal serialization (all built-in structs)
        /// </summary>
        /// <exception cref="NotImplementedException">When serialization does not resolve for any arg</exception>
        /// <param name="rpcId"></param>
        /// <param name="args"></param>
        public void PlayerRpc(byte rpcId, params object[] args)
        {
            var msg = Server.SerializeRpc(rpcId, GetMessage, args);
            SendMessage(msg, ReliabilityMode.Ordered);
        }

        /// <summary>
        /// Sends the args as an rpc to the client player.
        /// </summary>
        /// <param name="rpcId"></param>
        /// <param name="args"></param>
        public void PlayerRpc(byte rpcId, params INetSerializable[] args)
        {
            int size = 0;
            args.AllocSize(ref size);
            var msg = StartMessage(rpcId, ReliabilityMode.Ordered, size);
            INetSerializableExtensions.WriteParams(ref msg, args);
            SendMessage(msg, ReliabilityMode.Ordered);
        }

        /// <summary>
        /// send the rpc to all players except this one.
        /// Sends to all if this is Player.ServerPlayer
        /// </summary>
        /// <param name="rpcId"></param>
        /// <param name="args"></param>
        public void OtherPlayersRpc(byte rpcId, params object[] args)
        {
            if (this == ServerPlayer)
            {
                Server.AllPlayersRpc(rpcId, args);
            }
            else
            {
                Server.AllPlayersRpc(this, rpcId, args);
            }
        }

        NetMessage StartMessage(byte rpcId, ReliabilityMode mode, int size)
        {
            var msg = GetMessage(size + 2);
            msg.Write(RpcUtils.GetHeader(mode, BroadcastMode.Server, MsgType.Static));
            msg.Write(rpcId);
            return msg;
        }

        NetMessage GetMessage(int size)
        {
            return Connection.Server.GetMessage(size);
        }
    }
}