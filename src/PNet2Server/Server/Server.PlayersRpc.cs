﻿using System;
using PNet;

namespace PNetS
{
    public partial class Server
    {
        /// <summary>
        /// send the rpc to all players
        /// </summary>
        /// <param name="rpcId"></param>
        /// <param name="args"></param>
        /// <exception cref="NullReferenceException"></exception>
        public void AllPlayersRpc(byte rpcId, params object[] args)
        {
            var msg = SerializeRpc(rpcId, args);
            playerServer.SendToAllPlayers(msg, ReliabilityMode.Ordered);
        }

        /// <summary>
        /// send the rpc to all players except except
        /// </summary>
        /// <param name="except"></param>
        /// <param name="rpcId"></param>
        /// <param name="args"></param>
        /// <exception cref="NullReferenceException"></exception>
        public void AllPlayersRpc(Player except, byte rpcId, params object[] args)
        {
            var msg = SerializeRpc(rpcId, args);
            playerServer.SendToAllPlayersExcept(except, msg, ReliabilityMode.Ordered);
        }

        /// <summary>
        /// Serialize an rpc once, to be used in conjuction with Player.SendMessage. 
        /// YOU SHOULD CALL NetMessage.RecycleMessage WHEN YOU ARE FINISHED WITH IT TO REDUCE GARBAGE COLLECTION
        /// </summary>
        /// <param name="rpcId"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public NetMessage SerializeRpc(byte rpcId, params object[] args)
        {
            return SerializeRpc(rpcId, GetPlayerMessage, args);
        }

        internal NetMessage SerializeRpc(byte rpcId, Func<int, NetMessage> getMessage, params object[] args)
        {
            var size = 0;
            foreach (var arg in args)
            {
                if (arg == null)
                    throw new NullReferenceException("Cannot serialize null value");

                size += Serializer.SizeOf(arg);
            }

            var msg = getMessage(size + 2);
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Static));
            msg.Write(rpcId);


            foreach (var arg in args)
            {
                Serializer.Serialize(arg, msg);
            }
            return msg;
        }

        internal void SendToAll(Player except, NetMessage msg, ReliabilityMode reliability)
        {
            playerServer.SendToAllPlayersExcept(except, msg, reliability);
        }
        internal void SendToAll(NetMessage msg, ReliabilityMode reliability)
        {
            playerServer.SendToAllPlayers(msg, reliability);
        }
    }
}
