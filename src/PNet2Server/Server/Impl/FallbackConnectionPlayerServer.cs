﻿using System.Threading.Tasks;
using PNet;

namespace PNetS.Impl
{
    public class DualConnectionPlayerServer : APlayerServer
    {
        public override Server Server
        {
            get { return primary.Server; }
            internal set
            {
                primary.Server = value;
                backup.Server = value;
            }
        }

        private readonly APlayerServer primary;
        private readonly APlayerServer backup;

        public DualConnectionPlayerServer(APlayerServer primary, APlayerServer backup)
        {
            this.primary = primary;
            this.backup = backup;
        }

        protected internal override void AllowPlayerToConnect(Player player)
        {
            player.Connection.Server.AllowPlayerToConnect(player);
        }

        protected internal override void Disconnect(Player player, string reason)
        {
            player.Connection.Server.Disconnect(player, reason);
        }

        //this will be specifically used by the Server sending 'all' messages
        protected internal override NetMessage GetMessage(int length)
        {
            return primary.GetMessage(length);
        }

        protected internal override void Initialize()
        {
            //we specifically want the primary to initialize first
            primary.Initialize();
            backup.Initialize();
        }

        protected internal override void SendToAllPlayers(NetMessage msg, ReliabilityMode mode)
        {
            primary.SendToAllPlayers(msg, mode);
        }

        protected internal override void SendToAllPlayersExcept(Player player, NetMessage msg, ReliabilityMode mode)
        {
            primary.SendToAllPlayersExcept(player, msg, mode);
        }

        protected internal override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode, bool recycle)
        {
            player.Connection.Server.SendToPlayer(player, msg, mode, recycle);
        }

        protected internal override async Task Shutdown(string reason)
        {
            //we specifically want the backup to shutdown first, so new players don't attempt to use the backup
            await backup.Shutdown(reason);
            await primary.Shutdown(reason);
        }
    }
}
