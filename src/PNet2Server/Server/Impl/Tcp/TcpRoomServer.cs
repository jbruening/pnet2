﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using PNet;

namespace PNetS.Impl
{
    public class TcpRoomServer : ARoomServer
    {
        class ConnectionInfo : ARoomConnection
        {
            public TcpClient Client;
            public NetworkStream Stream;
            public readonly TaskCompletionSource<bool> AllowConnectCompletion = new TaskCompletionSource<bool>();
            public Room Room;

            public ConnectionInfo(ARoomServer server, TcpClient client, NetworkStream networkStream, Room room) : base(server)
            {
                Client = client;
                Stream = networkStream;
                Room = room;
            }

            public override string ToString()
            {
                return Client.Client.RemoteEndPoint.ToString();
            }
        }

        public class Configuration
        {
            public int? ListenPort { get; set; }
        }

        private TcpListener tcpListener;
        private CancellationTokenSource shutdownTokenSource = new CancellationTokenSource();

        private bool acceptEnabled;
        private bool tcpRunning;
        private readonly List<ConnectionInfo> connections = new List<ConnectionInfo>();

        private readonly Configuration configuration;

        public TcpRoomServer(Configuration configuration = null)
        {
            this.configuration = configuration;
        }

        protected internal override void Initialize()
        {
            acceptEnabled = true;
            tcpRunning = true;
            TcpListenerLoop();
        }

        private async void TcpListenerLoop()
        {
            await Task.Delay(0).ConfigureAwait(false);

            tcpListener = new TcpListener(IPAddress.Any, configuration?.ListenPort ?? Server.Configuration.RoomListenPort);
            tcpListener.Start();

            while (acceptEnabled)
            {
                try
                {
                    var client = await tcpListener.AcceptTcpClientAsync().ConfigureAwait(false);
                    HandleClientConnection(client);
                }
                catch (ObjectDisposedException ode)
                {
                    if (acceptEnabled)
                    {
                        Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
                    }
                    //not worried about the _roomListener being disposed
                }
                catch (Exception exp)
                {
                    Debug.LogException(exp);
                }

            }
        }

        private async void HandleClientConnection(TcpClient client)
        {
            var endpoint = client.Client.RemoteEndPoint;
            Debug.Log("Room connection request from {0}", endpoint);
            client.ReceiveBufferSize = 1024;
            client.ReceiveTimeout = 5000;
            client.SendBufferSize = 1024;
            client.NoDelay = true;
            client.LingerState = new LingerOption(true, 10);

            Room room = null;
            try
            {
                using (var networkStream = client.GetStream())
                {
                    var buffer = new byte[1024];
                    NetMessage dataBuffer = null;
                    var bufferSize = 0;
                    var lengthBuffer = new byte[2];
                    var bytesReceived = 0;
                    int readBytes;

                    var authMessages = new Queue<NetMessage>();
                    while (tcpRunning)
                    {
                        readBytes = await networkStream.ReadAsync(buffer, 0, buffer.Length, shutdownTokenSource.Token);
                        if (readBytes > 0)
                        {
                            var readMessage = NetMessage.GetMessages(buffer, readBytes, ref bytesReceived,
                                ref dataBuffer, ref lengthBuffer, ref bufferSize, authMessages.Enqueue);
                            if (readMessage >= 1)
                                break;
                        }
                        if (!client.Connected)
                            return;
                    }

                    var ci = new ConnectionInfo(this, client, networkStream, room);

                    string denyReason;
                    if (
                        !ApproveRoomConnection(ci, (IPEndPoint)client.Client.RemoteEndPoint,
                            authMessages.Count > 0 ? authMessages.Dequeue() : null, out denyReason, out room))
                    {
                        var dcMessage = GetMessage(denyReason.Length * 2 + 1);
                        dcMessage.Write(false);
                        dcMessage.WritePadBits();
                        dcMessage.Write(denyReason);
                        dcMessage.WriteSize();
                        await
                            networkStream.WriteAsync(dcMessage.Data, 0, dcMessage.LengthBytes,
                                shutdownTokenSource.Token);
                        return;
                    }
                    else
                    {
                        var cMessage = GetMessage(17);
                        cMessage.Write(true);
                        cMessage.WritePadBits();
                        cMessage.Write(room.Guid);
                        cMessage.WriteSize();
                        await
                            networkStream.WriteAsync(cMessage.Data, 0, cMessage.LengthBytes, shutdownTokenSource.Token);
                    }

                    
                    lock (connections)
                        connections.Add(ci);

                    AddRoom(room);

                    while (authMessages.Count > 0)
                        room.ConsumeData(authMessages.Dequeue());

                    while (tcpRunning && room.Running)
                    {
                        readBytes = await networkStream.ReadAsync(buffer, 0, buffer.Length, shutdownTokenSource.Token);
                        if (readBytes > 0)
                        {
                            NetMessage.GetMessages(buffer, readBytes, ref bytesReceived, ref dataBuffer,
                                ref lengthBuffer, ref bufferSize, room.ConsumeData);
                        }
                        else
                            EnsureConnected(ci);
                        if (!client.Connected)
                            return;
                    }
                }
            }
            catch (OperationCanceledException o)
            {
                Debug.Log("Operation canceled");
            }
            catch (ObjectDisposedException ode)
            {
                if (tcpRunning && ode.ObjectName != "System.Net.Sockets.NetworkStream")
                    Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
            }
            catch (SocketException se)
            {

            }
            catch (IOException ioe)
            {
                if (!(ioe.InnerException is SocketException))
                    Debug.LogException(ioe);
            }
            catch (Exception exp)
            {
                Debug.LogException(exp);
            }
            finally
            {
                Debug.Log("Closing room connection to {0}", endpoint);
                client.Close();

                if (room != null)
                {
                    RemoveRoom(room);
                    lock (connections)
                        connections.Remove(room.Connection as ConnectionInfo);
                    //todo: move all players in the room to a new room.
                }
            }
        }

        protected internal override NetMessage GetMessage(int length)
        {
            return NetMessage.GetMessageSizePad(length);
        }

        protected internal override async Task Shutdown(string reason)
        {
            acceptEnabled = false;
            tcpListener.Stop();
            shutdownTokenSource.Cancel();

            var shutReason = GetMessage(reason.Length * 2 + 6);
            shutReason.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Owner, MsgType.Internal));
            shutReason.Write(DandRRpcs.DisconnectMessage);
            shutReason.Write(reason);
            shutReason.WriteSize();

            await Task.WhenAll(LockRooms().Select(c => SendTcpMessage(c, shutReason, false)));

            NetMessage.RecycleMessage(shutReason);
            tcpRunning = false;
        }

        protected internal override void SendToRoom(Room room, NetMessage msg, ReliabilityMode mode)
        {
            msg.WriteSize();
            SendTcpMessage(room.Connection as ConnectionInfo, msg);
        }

        protected internal override void SendToOtherRooms(Room except, NetMessage msg, ReliabilityMode mode)
        {
            msg.WriteSize();
            var conns = LockRooms();
            var ci = except.Connection as ConnectionInfo;
            var tasks = new List<Task>(conns.Length);
            foreach (var c in conns)
            {
                if (c == null) continue;
                if (c == ci) continue;
                tasks.Add(SendTcpMessage(c, msg, false));
            }
            Task.WhenAll(tasks).ContinueWith(task => NetMessage.RecycleMessage(msg));
        }

        protected internal override void SendToAllRooms(NetMessage msg, ReliabilityMode mode)
        {
            msg.WriteSize();
            var conns = LockRooms();
            var tasks = new List<Task>(conns.Length);
            foreach (var c in conns)
            {
                tasks.Add(SendTcpMessage(c, msg, false));
            }
            Task.WhenAll(tasks).ContinueWith(task => NetMessage.RecycleMessage(msg));
        }

        private void EnsureConnected(ConnectionInfo connection)
        {
            var msg = GetMessage(2);
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            msg.Write(DandPRpcs.Ping);
            SendTcpMessage(connection, msg);
        }

        ConnectionInfo[] LockRooms()
        {
            lock (connections)
                return connections.ToArray();
        }

        async Task SendTcpMessage(ConnectionInfo connection, NetMessage msg, bool recycle = true)
        {
            try
            {
                var strm = connection.Stream;
                if (strm == null)
                    return;
                await strm.WriteAsync(msg.Data, 0, msg.LengthBytes);
                if (!connection.Client.Connected)
                    connection.Client.Close();
            }
            catch (ObjectDisposedException oe)
            {
                if (oe.ObjectName == "System.Net.Sockets.NetworkStream")
                { }
                else
                {
                    Debug.LogException(oe);
                    connection.Client.Close();
                }
            }
            catch (IOException ioe)
            {
                if (!(ioe.InnerException is SocketException))
                    Debug.LogException(ioe, "On {0}", connection.Room);
                connection.Client.Close();
            }
            catch (Exception e)
            {
                Debug.LogException(e, "On {0}", connection.Room);
                connection.Client.Close();
            }
            finally
            {
                if (recycle)
                    NetMessage.RecycleMessage(msg);
            }
        }
    }
}
