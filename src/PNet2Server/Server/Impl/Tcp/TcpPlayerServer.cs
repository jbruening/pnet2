﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using PNet;

namespace PNetS.Impl
{
    public class TcpPlayerServer : APlayerServer
    {
        class ConnectionInfo : APlayerConnection
        {
            public TcpClient Client { get; }
            public NetworkStream Stream { get; }
            public readonly TaskCompletionSource<bool> AllowConnectCompletion = new TaskCompletionSource<bool>();
            public Player Player;

            public ConnectionInfo(APlayerServer server, TcpClient client, NetworkStream networkStream) : base(server)
            {
                Client = client;
                Stream = networkStream;
            }

            public override string ToString()
            {
                return Client.Client.RemoteEndPoint.ToString();
            }
        }

        public class Configuration
        {
            public int? ListenPort { get; set; }
        }

        private TcpListener tcpListener;
        private CancellationTokenSource shutdownTokenSource = new CancellationTokenSource();

        private bool acceptEnabled;
        private bool tcpRunning;
        private readonly List<ConnectionInfo> connections = new List<ConnectionInfo>();

        private readonly Configuration configuration;

        public TcpPlayerServer(Configuration configuration = null)
        {
            this.configuration = configuration;
        }

        protected internal override void Initialize()
        {
            acceptEnabled = true;
            tcpRunning = true;
            TcpListenerLoop();
        }

        private async void TcpListenerLoop()
        {
            await Task.Delay(0).ConfigureAwait(false);

            tcpListener = new TcpListener(IPAddress.Any, configuration?.ListenPort ?? Server.Configuration.PlayerListenPort);
            tcpListener.Start();

            while (acceptEnabled)
            {
                try
                {
                    var client = await tcpListener.AcceptTcpClientAsync().ConfigureAwait(false);
                    HandleClientConnection(client);
                }
                catch (ObjectDisposedException ode)
                {
                    if (acceptEnabled)
                    {
                        Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
                    }
                    //not worried about the _clientListener being disposed...
                }
                catch (Exception exp)
                {
                    Debug.LogException(exp);
                }
            }
        }

        private async void HandleClientConnection(TcpClient client)
        {
            var endpoint = client.Client.RemoteEndPoint;
            Debug.Log("Player connection request from {0}", endpoint);
            client.ReceiveBufferSize = 1024;
            client.ReceiveTimeout = 5000;
            client.SendBufferSize = 1024;
            client.NoDelay = true;
            client.LingerState = new LingerOption(true, 10);

            ConnectionInfo ci = null;
            try
            {
                using (var networkStream = client.GetStream())
                {
                    var buffer = new byte[1024];
                    NetMessage dataBuffer = null;
                    var bufferSize = 0;
                    var lengthBuffer = new byte[2];
                    var bytesReceived = 0;
                    int readBytes;

                    var authMessages = new Queue<NetMessage>();
                    while (tcpRunning)
                    {
                        readBytes = await networkStream.ReadAsync(buffer, 0, buffer.Length, shutdownTokenSource.Token);
                        if (readBytes > 0)
                        {
                            var readMessage = NetMessage.GetMessages(buffer, readBytes, ref bytesReceived,
                                ref dataBuffer, ref lengthBuffer, ref bufferSize, authMessages.Enqueue);
                            if (readMessage >= 1)
                                break;
                        }
                        if (!client.Connected)
                            return;
                    }

                    ci = new ConnectionInfo(this, client, networkStream);

                    lock (connections)
                        connections.Add(ci);

                    if (authMessages.Count > 0)
                        PlayerAttemptingConnection(ci, (IPEndPoint)client.Client.RemoteEndPoint, player =>
                        {
                            ci.Player = player;
                            player.Status = ConnectionStatus.Connecting;
                        }, authMessages.Dequeue());
                    else
                        ci.Player.Disconnect("No authentication message sent");

                    var canConnect = await ci.AllowConnectCompletion.Task;
                    if (!canConnect) return;

                    ci.Player.Status = ConnectionStatus.Connected;
                    FinalizePlayerAdd(ci.Player);

                    Debug.Log("Client connected from {0}", client.Client.RemoteEndPoint);
                    //and drain the rest of messages that might have come after the auth
                    while (authMessages.Count > 0)
                        ci.Player.ConsumeData(authMessages.Dequeue());

                    while (tcpRunning)
                    {
                        readBytes = await networkStream.ReadAsync(buffer, 0, buffer.Length, shutdownTokenSource.Token);
                        if (readBytes > 0)
                        {
                            NetMessage.GetMessages(buffer, readBytes, ref bytesReceived, ref dataBuffer,
                                ref lengthBuffer, ref bufferSize, ci.Player.ConsumeData);
                        }
                        else
                            EnsureConnected(ci);
                        if (!client.Connected)
                            return;
                    }
                }
            }
            catch (OperationCanceledException o)
            {
                Debug.Log("Operation canceled");
            }
            catch (ObjectDisposedException ode)
            {
                if (tcpRunning && !ode.ObjectName.StartsWith("System.Net.Sockets"))
                    Debug.LogException(ode, "{0} disposed when it shouldn't have", ode.ObjectName);
            }
            catch (SocketException se)
            {

            }
            catch (IOException ioe)
            {
                if (!(ioe.InnerException is SocketException))
                    Debug.LogException(ioe);
            }
            catch (Exception exp)
            {
                Debug.LogException(exp);
            }
            finally
            {
                Debug.Log("Closing client connection to {0}", endpoint);
                client.Close();

                if (ci != null)
                {
                    ci.Player.Status = ConnectionStatus.Disconnecting;

                    lock (connections)
                        connections.Remove(ci);
                    ci.Player.Status = ConnectionStatus.Disconnected;
                    RemovePlayer(ci.Player);
                    ci.Player.Connection = null;
                }
            }
        }

        private void EnsureConnected(ConnectionInfo connection)
        {
            var msg = GetMessage(2);
            msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Server, MsgType.Internal));
            msg.Write(DandPRpcs.Ping);
            SendTcpMessage(connection, msg);
        }

        protected internal override NetMessage GetMessage(int length)
        {
            return NetMessage.GetMessageSizePad(length);
        }

        protected internal override async Task Shutdown(string reason)
        {
            acceptEnabled = false;
            tcpListener.Stop();
            shutdownTokenSource.Cancel();

            var shutReason = GetMessage(reason.Length * 2 + 6);
            shutReason.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Owner, MsgType.Internal));
            shutReason.Write(DandRRpcs.DisconnectMessage);
            shutReason.Write(reason);
            shutReason.WriteSize();

            await Task.WhenAll(LockPlayers().Select(c => SendTcpMessage(c, shutReason, false)));

            NetMessage.RecycleMessage(shutReason);
            tcpRunning = false;
        }

        protected internal override async void AllowPlayerToConnect(Player player)
        {
            var msg = GetMessage(3);
            msg.Write(true);
            msg.WritePadBits();
            msg.Write(player.Id);
            msg.WriteSize();

            var ci = player.Connection as ConnectionInfo;
            try
            {
                var strm = ci.Stream;
                if (strm != null)
                    await ci.Stream.WriteAsync(msg.Data, 0, msg.LengthBytes);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                ci.Client.Close();
                ci.AllowConnectCompletion.TrySetResult(false);
                return;
            }
            finally
            {
                NetMessage.RecycleMessage(msg);
            }
            ci.AllowConnectCompletion.TrySetResult(true);
        }

        protected internal override async void Disconnect(Player player, string reason)
        {
            var msg = GetMessage(reason.Length * 2 + 5);
            var ci = player.Connection as ConnectionInfo;
            if (ci.AllowConnectCompletion.TrySetResult(false))
            {
                //we're actually denying connection, not sending a disconnect
                msg.Write(false);
                msg.WritePadBits();
                msg.Write(reason);
            }
            else
            {
                msg.Write(RpcUtils.GetHeader(ReliabilityMode.Ordered, BroadcastMode.Owner, MsgType.Internal));
                msg.Write(DandPRpcs.DisconnectMessage);
                msg.Write(reason);
            }
            msg.WriteSize();
            try
            {
                var strm = ci.Stream;
                if (strm != null)
                    await strm.WriteAsync(msg.Data, 0, msg.LengthBytes);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            await Task.Delay(5000);
            NetMessage.RecycleMessage(msg);
            ci.Client.Close();
            ci.Stream.Dispose();
        }

        protected internal override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode, bool recycle)
        {
            msg.WriteSize();
            SendTcpMessage(player.Connection as ConnectionInfo, msg, recycle);
        }

        protected internal override void SendToAllPlayers(NetMessage msg, ReliabilityMode mode)
        {
            msg.WriteSize();
            var conns = LockPlayers();
            var tasks = new List<Task>(conns.Length);
            foreach (var c in conns)
            {
                tasks.Add(SendTcpMessage(c, msg, false));
            }
            Task.WhenAll(tasks).ContinueWith(task => NetMessage.RecycleMessage(msg));
        }

        ConnectionInfo[] LockPlayers()
        {
            lock (connections)
                return connections.ToArray();
        }

        protected internal override void SendToAllPlayersExcept(Player player, NetMessage msg, ReliabilityMode mode)
        {
            msg.WriteSize();
            var conns = LockPlayers();
            var ci = player.Connection as ConnectionInfo;
            var tasks = new List<Task>(conns.Length);
            foreach (var pl in conns)
            {
                if (pl == null) continue;
                if (pl == ci) continue;
                tasks.Add(SendTcpMessage(pl, msg, false));
            }
            Task.WhenAll(tasks).ContinueWith(task => NetMessage.RecycleMessage(msg));
        }

        async Task SendTcpMessage(ConnectionInfo connection, NetMessage msg, bool recycle = true)
        {
            try
            {
                var strm = connection.Stream;
                if (strm == null)
                    return;
                await strm.WriteAsync(msg.Data, 0, msg.LengthBytes);
                if (!connection.Client.Connected)
                    connection.Client.Close();
            }
            catch (ObjectDisposedException oe)
            {
                if (oe.ObjectName == "System.Net.Sockets.NetworkStream")
                { }
                else
                {
                    Debug.LogException(oe);
                    connection.Client.Close();
                }
            }
            catch (IOException ioe)
            {
                if (!(ioe.InnerException is SocketException))
                    Debug.LogException(ioe, "On {0}", connection.Player);
                connection.Client.Close();
            }
            catch (Exception e)
            {
                Debug.LogException(e, "On {0}", connection.Player);
                connection.Client.Close();
            }
            finally
            {
                if (recycle)
                    NetMessage.RecycleMessage(msg);
            }
        }
    }
}
