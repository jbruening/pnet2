﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Lidgren.Network;
using PNet;

namespace PNetS.Impl
{
    public class LidgrenPlayerServer : APlayerServer
    {    
        class LidgrenPlayerConnection : APlayerConnection
        {
            public NetConnection Connection { get; }

            public LidgrenPlayerConnection(APlayerServer server, NetConnection connection)
                : base(server)
            {
                Connection = connection;
            }
        }

        private NetServer PlayerServer;

        protected internal override void Initialize()
        {
            var playerConfig = new NetPeerConfiguration(Server.Configuration.AppIdentifier);

            playerConfig.AutoFlushSendQueue = true;
            playerConfig.Port = Server.Configuration.PlayerListenPort;
            playerConfig.MaximumConnections = Server.Configuration.MaximumPlayers;
            playerConfig.SetMessageTypeEnabled(NetIncomingMessageType.ConnectionApproval, true);

#if DEBUG
            Debug.Log("Debug build. Simulated latency and packet loss/duplication is enabled.");
            playerConfig.SimulatedLoss = 0.001f;
            playerConfig.SimulatedDuplicatesChance = 0.001f;
            playerConfig.SimulatedMinimumLatency = 0.1f;
            playerConfig.SimulatedRandomLatency = 0.01f;
#endif

            PlayerServer = new NetServer(playerConfig);
            PlayerServer.RegisterReceivedCallback(ReceivedCallback, new SynchronizationContext());
            PlayerServer.Start();
        }

        private void ReceivedCallback(object peer)
        {
            var netPeer = peer as NetServer;
            if (netPeer == null) return;

            var lmsg = netPeer.ReadMessage();

            var msgType = lmsg.MessageType;
            var sender = lmsg.SenderEndPoint;
            var sConn = lmsg.SenderConnection;
            var seq = lmsg.SequenceChannel;
            var msg = NetMessage.GetMessage(lmsg.Data.Length);
            lmsg.Clone(msg);
            msg.Sender = lmsg.SenderConnection;
            netPeer.Recycle(lmsg);

            if (msgType == NetIncomingMessageType.Data)
            {
                var player = GetPlayer(sConn);
                if (player != null)
                    ConsumeData(player, msg);
                else
                {
                    Debug.LogError("Unknown player {0} sent {1} bytes of data", sender, msg.LengthBytes);
                    sConn.Disconnect(DtoPMsgs.UnknownPlayer);
                }
            }
            else if (msgType == NetIncomingMessageType.DebugMessage)
            {
                Debug.Log(msg.ReadString());
            }
            else if (msgType == NetIncomingMessageType.ConnectionApproval)
            {
                PlayerAttemptingConnection(new LidgrenPlayerConnection(this, sConn), sConn.RemoteEndPoint, player => sConn.Tag = player, msg);
            }
            else if (msgType == NetIncomingMessageType.StatusChanged)
            {
                var status = (NetConnectionStatus)msg.ReadByte();
                var statusReason = msg.ReadString();
                var player = GetPlayer(sConn);
                if (player != null)
                {
                    player.Status = status.ToPNet();
                    if (status == NetConnectionStatus.Disconnecting || status == NetConnectionStatus.Disconnected)
                    {
#if DEBUG
                        if (statusReason == "unequaldisconnect")
                            RemovePlayerNoNotify(player);
                        else
#endif
                        RemovePlayer(player);
                        sConn.Tag = null;
                    }
                    if (status == NetConnectionStatus.Connected)
                    {
                        FinalizePlayerAdd(player);
                    }
                    Debug.Log("Player status: {0} {2}, {1}", player.Status, statusReason, player.Id);
                }
                else if (status == NetConnectionStatus.RespondedAwaitingApproval)
                {
                    //eh.
                }
                else
                {
                    Debug.Log("Unknown player {0} status: {1}, {2}", sConn, status, statusReason);
                }
            }
            else if (msgType == NetIncomingMessageType.WarningMessage)
            {
                var str = msg.ReadString();
                if (!str.StartsWith("Received unhandled library message Acknowledge"))
                    Debug.LogWarning(str);
            }
            else if (msgType == NetIncomingMessageType.Error)
            {
                Debug.LogException(new Exception(msg.ReadString()) { Source = "Server.Lidgren.PlayerCallback [Errored Lidgren Message]" }); //this should really never happen...
            }

            NetMessage.RecycleMessage(msg);
        }

        protected internal override void SendToAllPlayers(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = PlayerServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);
            PlayerServer.SendToAll(lmsg, null, method, seq);
        }
        protected internal override void SendToAllPlayersExcept(Player player, NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = PlayerServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);
            PlayerServer.SendToAll(lmsg, GetLConnection(player), method, seq);
        }

        protected internal override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode, bool recycle)
        {
            var lmsg = PlayerServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            if (recycle)
                NetMessage.RecycleMessage(msg);

            int seq;
            var method = mode.PlayerDelivery(out seq);
            PlayerServer.SendMessage(lmsg, GetLConnection(player), method, seq);
        }

        protected internal override void AllowPlayerToConnect(Player player)
        {
            var lmsg = PlayerServer.CreateMessage(2);
            lmsg.Write(player.Id);
            GetLConnection(player).Approve(lmsg);
        }

        protected internal override void Disconnect(Player player, string reason)
        {
            GetLConnection(player).Disconnect(reason);
        }

        protected internal override NetMessage GetMessage(int length)
        {
            return NetMessage.GetMessage(length);
        }

        protected internal override async Task Shutdown(string reason)
        {
            PlayerServer.Shutdown(reason);

            while (PlayerServer.Status != NetPeerStatus.NotRunning)
            {
                await Task.Delay(10);
            }
        }

        

        private static NetConnection GetLConnection(Player player)
        {
            return (player.Connection as LidgrenPlayerConnection).Connection;
        }

        private static Player GetPlayer(NetConnection connection)
        {
            return connection.Tag as Player;
        }
    }
}
