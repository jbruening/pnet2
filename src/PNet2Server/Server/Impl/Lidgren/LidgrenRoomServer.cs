﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Lidgren.Network;
using PNet;

namespace PNetS.Impl
{
    public class LidgrenRoomServer : ARoomServer
    {
        class LidgrenRoomConnection : ARoomConnection
        {
            public NetConnection Connection { get; }

            public LidgrenRoomConnection(LidgrenRoomServer server, NetConnection connection)
                : base(server)
            {
                Connection = connection;
            }
        }
        
        internal NetServer RoomServer { get; private set; }

        protected internal override void Initialize()
        {
            //set up room server
            var roomConfig = new NetPeerConfiguration(Server.Configuration.AppIdentifier + "DPT");

            roomConfig.AutoFlushSendQueue = true;
            roomConfig.Port = Server.Configuration.RoomListenPort;
            roomConfig.MaximumConnections = Server.Configuration.MaximumRooms;
            roomConfig.SetMessageTypeEnabled(NetIncomingMessageType.ConnectionApproval, true);

            RoomServer = new NetServer(roomConfig);
            RoomServer.RegisterReceivedCallback(ReceivedCallback, new SynchronizationContext());
            RoomServer.Start();
        }

        private void ReceivedCallback(object peer)
        {
            var netPeer = peer as NetServer;
            if (netPeer == null) return;

            var lmsg = netPeer.ReadMessage();

            var msgType = lmsg.MessageType;
            var sender = lmsg.SenderEndPoint;
            var sConn = lmsg.SenderConnection;
            var seq = lmsg.SequenceChannel;
            var msg = NetMessage.GetMessage(lmsg.Data.Length);
            lmsg.Clone(msg);
            msg.Sender = lmsg.SenderConnection;
            netPeer.Recycle(lmsg);

            if (msgType == NetIncomingMessageType.Data)
            {
                var room = GetRoom(sConn);
                if (room != null)
                    ConsumeData(room, msg);
                else
                {
                    Debug.LogError("Unknown room {0} sent {1} bytes of data", sender, msg.LengthBytes);
                    sConn.Disconnect(DtoRMsgs.UnknownRoom);
                }
            }
            else if (msgType == NetIncomingMessageType.DebugMessage)
            {
                Debug.Log(msg.ReadString());
            }
            else if (msgType == NetIncomingMessageType.ConnectionApproval)
            {
                LidgrenApproveRoomConnection(sConn, sender, msg);
            }
            else if (msgType == NetIncomingMessageType.StatusChanged)
            {
                var status = (NetConnectionStatus)msg.ReadByte();
                var statusReason = msg.ReadString();

                if (status == NetConnectionStatus.Connected)
                {
                    var room = sConn.Tag as Room;
                    if (room == null)
                    {
                        Debug.LogError($"A connection joined from {sConn}, but it did not have a room set in the Tag property");
                        sConn.Disconnect(DtoRMsgs.UnknownRoom);
                    }
                    else
                        AddRoom(room);
                }
                else if (status == NetConnectionStatus.Disconnecting || status == NetConnectionStatus.Disconnected)
                {
                    var oldRoom = GetRoom(sConn);
                    if (oldRoom != null)
                    {
                        RemoveRoom(oldRoom);
                        sConn.Tag = null;

                        //todo: move all players in the room to a new room.
                    }
                }

                Debug.Log("Room status: {0}, {1}", status, statusReason);
            }
            else if (msgType == NetIncomingMessageType.WarningMessage)
            {
                Debug.LogWarning(msg.ReadString());
            }
            else if (msgType == NetIncomingMessageType.Error)
            {
                Debug.LogException(new Exception(msg.ReadString()) { Source = "Server.Lidgren.RoomCallback [Errored Lidgren Message]" }); //this should really never happen...
            }

            NetMessage.RecycleMessage(msg);
        }

        private void LidgrenApproveRoomConnection(NetConnection sConn, IPEndPoint sender, NetMessage msg)
        {
            Room room;
            string denyReason;
            if (!ApproveRoomConnection(new LidgrenRoomConnection(this, sConn), sender, msg, out denyReason, out room))
            {
                sConn.Deny(denyReason);
                return;
            }

            var gbytes = room.Guid.ToByteArray();
            var gmsg = RoomServer.CreateMessage(gbytes.Length);
            gmsg.Write(gbytes);
            sConn.Approve(gmsg);
            sConn.Tag = room;
        }

        Room GetRoom(NetConnection connection)
        {
            return connection.Tag as Room;
        }

        protected internal override NetMessage GetMessage(int length)
        {
            return NetMessage.GetMessage(length);
        }

        protected internal override async Task Shutdown(string reason)
        {
            RoomServer.Shutdown(reason);

            while (RoomServer.Status != NetPeerStatus.NotRunning)
            {
                await Task.Delay(10);
            }
        }

        protected internal override void SendToRoom(Room room, NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = RoomServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            var method = mode.RoomDelivery();
            RoomServer.SendMessage(lmsg, GetLConnection(room), method);
        }

        protected internal override void SendToOtherRooms(Room except, NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = RoomServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            var method = mode.RoomDelivery();
            RoomServer.SendToAll(lmsg, GetLConnection(except), method, 0);
        }

        protected internal override void SendToAllRooms(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = RoomServer.CreateMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);

            var method = mode.RoomDelivery();
            RoomServer.SendToAll(lmsg, method);
        }

        private static NetConnection GetLConnection(Room room)
        {
            return (room.Connection as LidgrenRoomConnection).Connection;
        }
    }
}
