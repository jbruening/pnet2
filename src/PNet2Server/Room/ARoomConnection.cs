﻿namespace PNetS
{
    public abstract class ARoomConnection
    {
        public ARoomServer Server { get; }

        protected internal ARoomConnection(ARoomServer server)
        {
            Server = server;
        }
    }
}