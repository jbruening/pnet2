﻿using PNet;
using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading.Tasks;

namespace PNetS
{
    public abstract class ARoomServer
    {
        public Server Server { get; internal set; }

        protected internal abstract void Initialize();
        protected internal abstract NetMessage GetMessage(int length);
        protected internal abstract Task Shutdown(string reason);

        protected internal abstract void SendToRoom(Room room, NetMessage msg, ReliabilityMode mode);
        protected internal abstract void SendToOtherRooms(Room except, NetMessage msg, ReliabilityMode mode);
        protected internal abstract void SendToAllRooms(NetMessage msg, ReliabilityMode mode);

        protected void ConsumeData(Room room, NetMessage msg)
        {
            room.ConsumeData(msg);
        }

        protected void AddRoom(Room room)
        {
            Server.AddRoom(room);
            Debug.Log("Room connected: {1} - {0} @ {2}", room.Connection, room.RoomId, room.Address);
        }

        protected void RemoveRoom(Room room)
        {
            Server.RemoveRoom(room);
        }

        #region password authorization cooldown
        private readonly ConcurrentDictionary<IPEndPoint, DateTime> _passCooldowns = new ConcurrentDictionary<IPEndPoint, DateTime>();

        private bool CheckPassCooldown(IPEndPoint sender)
        {
            DateTime existing;
            if (!_passCooldowns.TryGetValue(sender, out existing))
                return true;
            if (DateTime.Now > existing)
            {
                _passCooldowns.TryRemove(sender, out existing);
                return true;
            }
            //if they're checking while still in a cooldown state, let's refresh it.
            _passCooldowns[sender] = DateTime.Now + TimeSpan.FromSeconds(2);
            return false;
        }

        private void AddPassCooldown(IPEndPoint sender)
        {
            _passCooldowns[sender] = DateTime.Now + TimeSpan.FromSeconds(2);
        }
        #endregion

        protected bool ApproveRoomConnection(ARoomConnection connection, IPEndPoint sender, NetMessage msg, out string denyReason, out Room room)
        {
            room = null;
            string roomId;
            if (msg == null)
            {
                Debug.LogWarning("Denied room connection to {0} - no auth message", sender);
                denyReason = DtoRMsgs.NoRoomId;
                return false;
            }

            if (!msg.ReadString(out roomId))
            {
                Debug.LogWarning("Denied room connection to {0} - no room id", sender);
                denyReason = DtoRMsgs.NoRoomId;
                return false;
            }

            int iAuthType;
            if (!msg.ReadInt32(out iAuthType))
            {
                Debug.LogWarning("Denied room connection to {0} - didn't send a room auth type. Are they an old version?", sender);
                denyReason = DtoRMsgs.NotAllowed + " - no authtype";
                return false;
            }
            var authType = (RoomAuthType)iAuthType;

            string authData;
            if (!msg.ReadString(out authData))
            {
                Debug.LogWarning("Denied room connection to {0} - didn't send auth data. Are they an old version?", sender);
                denyReason = DtoRMsgs.NotAllowed + " - no authdata";
                return false;
            }

            string userDefinedAuthData;
            if (!msg.ReadString(out userDefinedAuthData))
            {
                Debug.LogWarning("Denied room connection to {0} - didn't send udef auth data. Are they an old version?", sender);
                denyReason = DtoRMsgs.NotAllowed + " - no udef authdata";
                return false;
            }

            switch (authType)
            {
                case RoomAuthType.AllowedHost:
                    break;
                case RoomAuthType.AllowedToken:
                    //we skip even checking the password if they're in a bad password cooldown state
                    if (CheckPassCooldown(sender))
                    {
                        if (Server.AllowedPasswords.Contains(authData))
                        {
                            goto Approved;
                        }
                        AddPassCooldown(sender);
                    }
                    Debug.LogWarning("Room {0} tried to auth with password {1}, but it wasn't valid", sender, authData);
                    break;
                default:
                    denyReason = DtoRMsgs.NotAllowed + " - unrecognized authtype";
                    Debug.LogWarning("Denied room connection to {0} - sent {1} for room auth type, which isn't recognized", sender, authType);
                    return false;
            }

            //we'll always check with allowed hosts
            if (!Server.AllowedRoomHosts.Contains(sender.Address))
            {
                denyReason = DtoRMsgs.NotAllowed + " - " + sender.Address;
                Debug.LogWarning("Denied room connection to {0}. Wasn't on allowed hosts list", sender);
                return false;
            }

        Approved:
            var port = msg.ReadInt32();
            var maxPlayers = msg.ReadInt32();
            string supAddr;
            IPAddress supIp = null;
            if (msg.ReadString(out supAddr) && !string.IsNullOrWhiteSpace(supAddr))
            {
                if (!IPAddress.TryParse(supAddr, out supIp))
                {
                    supIp = null;
                }
            }
            supIp = supIp ?? sender.Address;

            room = new Room(Server, connection, roomId, Guid.NewGuid(), new IPEndPoint(supIp, port))
            {
                MaxPlayers = maxPlayers,
                UserDefinedAuthData = userDefinedAuthData
            };

            denyReason = null;
            return true;
        }
    }
}
