﻿using System;
using System.Collections.Generic;
using System.Threading;
using Lidgren.Network;
using PNet;
using PNetS;
using PNetR;
using ServerDispatchServer = PNetS.Impl.LidgrenDispatchServer;
using RoomRoomServer = PNetR.Impl.LidgrenRoomServer;
using RoomDispatchClient = PNetR.Impl.LidgrenDispatchClient;

namespace ExampleServer
{
    class Program
    {
        public static PNetS.Server ServerServer;
        public static PNetS.Room ServerRoom;
        public static PNetR.Room RoomRoom;

        private static void ServerOnRoomAdded(PNetS.Room room)
        {
            ServerRoom = room;
        }
        private static void ServerOnVerifyPlayer(PNetS.Player player, NetMessage netMessage)
        {
            player.AllowConnect();
        }
        private static void ServerOnPlayerAdded(PNetS.Player player)
        {
            player.ChangeRoom(ServerRoom);
        }
        private static void RoomOnPlayerAdded(PNetR.Player player)
        {
            PNet.Structs.Vector3F zero = new PNet.Structs.Vector3F(0, 0, 0);
            NetworkView view = RoomRoom.Instantiate("Cube", zero, zero, player);
            view.ReceivedStream += RoomNWOnReceivedStream;
            player.UserData = view;
        }
        private static void RoomNWOnReceivedStream(NetMessage netMassage, PNetR.Player player)
        {
            NetworkView view = player.UserData as NetworkView;
            view.SendStream(netMassage);
        }

        static void Main(string[] args)
        {
            PNetS.Debug.Logger = new DefaultConsoleLogger();
            PNetR.Debug.Logger = new DefaultConsoleLogger();

            ServerServer = new PNetS.Server(new ServerDispatchServer());
            ServerServer.RoomAdded += ServerOnRoomAdded;
            ServerServer.VerifyPlayer += ServerOnVerifyPlayer;
            ServerServer.PlayerAdded += ServerOnPlayerAdded;
            var dispatchConfig = new PNetS.NetworkConfiguration(roomListenPort: Properties.Settings.Default.ServerRoomListenPort,
                playerListenPort: Properties.Settings.Default.ServerPlayerListenPort);
            ServerServer.Initialize(dispatchConfig);

            var roomConfig = new PNetR.NetworkConfiguration(maximumPlayers: 9999,
                listenPort: Properties.Settings.Default.RoomPlayerListenPort,
                listenAddress: "127.0.0.1",
                dispatcherPort: Properties.Settings.Default.RoomServerListenPort);
            RoomRoom = new PNetR.Room(roomConfig, new RoomRoomServer(), new RoomDispatchClient());
            RoomRoom.PlayerAdded += RoomOnPlayerAdded;
            RoomRoom.StartConnection();
            
            Console.WriteLine("Server is running");
            while(true)
            {
                RoomRoom.ReadQueue();
                Thread.Sleep(20);

                //if you wanted, you could also process other commands here to pass to the server/rooms.
            }

            RoomRoom.Shutdown();
            ServerServer.Shutdown();

            //give plenty of time for the server thread to close nicely
            Thread.Sleep(50);
        }
    }
}
